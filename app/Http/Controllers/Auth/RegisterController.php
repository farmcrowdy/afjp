<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserCode;
use App\Models\UserRole;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function authenticate(Request $request)
    {
        //format the number to remove the first 0, should it be received.
        $phone_number = $request->phone_number;
        $defaultRequests = ['register', 'registerWithPhone', 'registerWithEmail', 'login', 'resendOTP', 'resendEmailOTP', 'loginWithEmail'];
        // request_type : register, login or resendOTP
        $password = $request->password;


        if (!in_array($request->request_type, $defaultRequests)) {
            return Helper::jsonResponse(false, 'Invalid request type sent!', 400);
        }

        //switch between the type received
        switch ($request->request_type) {
            case 'resendOTP':
                $isEmpty = ['Phone Number' => $phone_number];
                if(\Helper::checkEmptyFields($isEmpty)) {
                    return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty), '400');
                }
                return $this->resendOTP($phone_number);
                break;

            case 'login':
                $isEmpty = ['Phone Number' => $phone_number, 'request type' => $request->request_type];
                if(\Helper::checkEmptyFields($isEmpty)) {
                    return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty), '400');
                }
                return $this->login($request);
                break;
            case 'registerWithPhone':
                // Check if inputs are empty
                $isEmpty = [
                    'phone number' => $phone_number,
                    'request type' => $request->request_type,
                    'First Name' => $request->first_name,
                    'Last Name' => $request->last_name,
                    'Farm Location State' => $request->farm_state_id,
                    'Role' => $request->role_id,
                    'Password' => $request->password,
                ];

                $input = $request->all();

                $validator = Validator::make($input, [
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'farm_state_id' => 'required',
//                    'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'password' => 'required|confirmed'
                ]);

                if($validator->fails()){
                    return $this->sendError('Validation Error.', $validator->errors());
                }


                if($request->avatar) {
                    $imageName = 'user_'.time().'.'.request()->avatar->getClientOriginalExtension();
                    $request->avatar->storeAs('public/images/avatar/', $imageName);
                } else {
                    $imageName = 'dummy.jpg';
                }

                $first_name = $request->first_name;
                $last_name = $request->last_name;
                $farm_state_id = $request->farm_state_id;
                $role_id = $request->role_id;
                $password = $request->password;

                if(\Helper::checkEmptyFields($isEmpty)) {
                    return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty), '400');
                }

                $referral_code = $request->referral_code;
                return $this->phoneSignUp(
                    $phone_number,
                    $imageName,
                    $first_name,
                    $last_name,
                    $farm_state_id,
                    $role_id,
                    $password,
                    $referral_code
                );
                break;
//            default:
//                $this->register($request->post());
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    private function phoneSignUp(
        string $phone,
        string $imageName,
        string $first_name,
        string $last_name,
        int $farm_state_id,
        int $role_id,
        string $password,
        $referralCode = null)
    {

        if(\Helper::validatePhone($phone)) return \Helper::validatePhone($phone);
        //format the phone number
        $formattedPhone = '+234'.(int)$phone;
        $Phone = User::checkIfExists('phone_number', $formattedPhone);
        if($Phone && $Phone->is_phone_verified)return \Helper::jsonResponse(false, 'An account with this number already exists.', 400);
        //if phone number was not verified

        if($Phone && !$Phone->is_phone_verified) {
            //send otp to the phone and email
            $OTP = UserCode::createNewOTP($Phone->id, $formattedPhone, "Do not send otp to mobile");
        } else {
            //create the new user and create a new  token
            $User = self::createNewUserWithPhone(
                $formattedPhone,
                $imageName,
                $first_name,
                $last_name,
                $farm_state_id,
                $role_id,
                $password
            );
            $OTP = UserCode::createNewOTP($User->id, $formattedPhone, 'Do not send otp to mobile');
        }
        return \Helper::jsonResponse(true, 'Please check your phone for the verification code.', 200, $OTP);
    }

    /**
     * @param $phone_number
     * @return User
     */
    public static function createNewUserWithPhone(
        string $phone_number,
        string $imageName,
        string $first_name,
        string $last_name,
        int $farm_state_id,
        int $role_id,
        string $password,
        string $email = null
    )
    {
        $newUser = new User();
        $newUser->phone_number = $phone_number;
        $newUser->first_name = $first_name;
        $newUser->last_name = $last_name;
        $newUser->farm_state_id = $farm_state_id;
        $newUser->avatar = $imageName;
        $newUser->role_id = $role_id;
        $newUser->password = User::protectPassword($password);
        if ($email) $newUser->email = $email;
        $newUser->save();

        $userRole = new UserRole();
        $userRole->user_id = $newUser->id;
        $userRole->role_id = 1;
        $userRole->save();

        return $newUser;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request){
        $phone_number = $request->phone_number;
        $password = $request->password;

        $isEmpty = ['phone' => $phone_number, 'password'=>$password];

        if(\Helper::checkEmptyFields($isEmpty))return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty),400);
        if(is_null($phone_number))return \Helper::jsonResponse(false, 'Invalid phone number entered.', 400);
        if(strlen($phone_number) > 11 || strlen($phone_number) < 11)return \Helper::jsonResponse(false, 'Phone number should contain only 11 digits.', 400);
        $formattedPhone = '+234'.(int)$phone_number;//convert to Nigeria
        $loginData = ['phone_number'=>$formattedPhone, 'password'=>$password];
        //validates the login credentials
        if(!Auth::attempt($loginData)) return \Helper::jsonResponse(false, 'Invalid login credentials.', 400);
        $user = User::where('phone_number', $formattedPhone)->first();
        $user->avatar = asset('storage/images/avatar/' . $user->avatar);
        //create a new access token when login is successful
        $access_token = \Helper::createAccessToken($user);

        /**
         * Do other things to initialize the experience of a user
         */
        $data = [
            'access_token' => $access_token,
            'user' => $user
        ];

        //deletes any other access tokens from the database already created before this.
        \Helper::deleteEarliestUserAccessToken($user->id);

        return \Helper::jsonResponse(true, 'Login successful', 200, $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function logOut(Request $request){
        $User = Auth::user()->token();
        $User->revoke(); //revoke the user data

        return Helper::jsonResponse(true, 'Logout is successful.', 200);
    }

    /**
     * The OTP Verification section
     * @param $phone_number
     * @return JsonResponse|int|string
     * @throws TypeException
     */
    private function resendOTP($phone_number) {
        $USER_NAME = null;
        $column = null;
        if(is_numeric($phone_number)){
            if(\Helper::validatePhone($phone_number)) return \Helper::validatePhone($phone_number);
            //format the phone number
            $USER_NAME = '+234'.(int)$phone_number;
            $column = 'phone_number';
        }

        $User = User::where($column, $USER_NAME)->first();
        if(!$User) return \Helper::jsonResponse(false, "Invalid phone number entered.", 400);
        $userID = $User->id;
        $OTP = null;
        switch ($column){
            case 'phone_number':
                $OTP = UserCode::createNewOTP($User->id, $USER_NAME);
//                $sms = \Helper::sendOTP($USER_NAME, $OTP);
                break;

            default:
                return \Helper::jsonResponse(false, "An error occurred.", 400);
        }

        return \Helper::jsonResponse(true, 'OTP sent!', 200,
            ['otp' => $OTP, 'phone_number' => $phone_number]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function verifyOTP(Request $request){
        $otp = $request->otp_code;
        $phone = $request->phone;

        if(!$otp){return \Helper::jsonResponse(false, 'The OTP field is required.', 400);}

        $UserCode = UserCode::where('otp', $otp)->latest()->first();

        if(!$UserCode){return \Helper::jsonResponse(false, 'Invalid OTP sent!', 400);}
        if((int)$UserCode->is_used){return \Helper::jsonResponse(false, 'OTP has already been used.', 400);}

        //check if OTP has expired after 30 seconds
        $updated_at = Carbon::make($UserCode->updated_at);
        if(\Helper::validateOTP($updated_at))return \Helper::jsonResponse(false, 'OTP already expired, click on Resend Code',400);

        //update the user otp , create an access token and get the user data
        $UserCode->is_used = 1;
        $UserCode->save();
        //..................................................................................................................
        $User = User::where('id', $UserCode->user_id)->first();
        if(!$User->is_phone_verified){
            $User->is_phone_verified = 1;
            $User->save();
        }
        //..................................................................................................................
        $AccessData = $this->returnAccessData($User);
        return \Helper::jsonResponse(true, 'Success', 200, $AccessData);
    }

    /**
     * create the access login data
     * @param $UserModel
     * @return array
     */
    private function returnAccessData (User $UserModel){
        $AccessToken = $this->createAccessToken($UserModel);
        $UserData = [
            'access_token' => $AccessToken,
            'user' => $UserModel,
        ];
        return $UserData;
    }

    /**
     * create a user access token
     * @param User $user
     * @return string
     */
    private function createAccessToken(User $user){
        try {
            return $AccessToken = \Helper::createAccessToken($user);
        } catch (\Exception $e) {
            echo $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function requestPasswordChange(Request $request){
        if($request->phone_number) {
            $phone = $request->phone_number;
            $isEmpty = ['phone number' => $phone];
            if(\Helper::checkEmptyFields($isEmpty)) return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty));
            if(\Helper::validatePhone($phone)) return \Helper::validatePhone($phone);
            $formattedPhone = "+234".(int)$phone;
            $User = User::getUser($formattedPhone);
            if(!$User) return \Helper::jsonResponse(false, "Phone number is invalid.");
            $OTP = UserCode::createNewOTP($User->id, $formattedPhone);
        } elseif($request->email) {
            $email = $request->email;
            $isEmpty = ['Email address' => $email];
            if(\Helper::checkEmptyFields($isEmpty)) return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty));
            $User = User::getUser($email);
            if(!$User) return \Helper::jsonResponse(false, "Email address is invalid.");
            $OTP = UserCode::createNewEmailOTP($User, 'Do not send otp to mobile');
        }

        return \Helper::jsonResponse(true, "New OTP sent!", 200, $OTP);
    }

    /**
     * validate a Driver's phone number for changing a default password
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePassword(Request $request){
        $phone = $request->phone_number;
        $password = $request->password;
        $new_password = $request->new_password;

        $isEmpty = [
            'phone_number' => $phone,
            'password' => $password,
            'new_password' => $new_password
        ];
        if(\Helper::checkEmptyFields($isEmpty)) return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty));
        $User = User::checkIfExists('phone_number', $phone);
        if(!$User) return \Helper::jsonResponse(false, "User Error.", 500);

        if (Hash::check($password, $User->password)) {
            if (Hash::check($new_password, $User->password)) {
                return \Helper::jsonResponse(false, 'Seems you are using an old password, try another password.', 400);
            }

            //update credentials
            User::updateRecord($User->id, ['password'=> $new_password, 'is_default_pass_changed'=> 'Yes']);
            UserCode::updateCodeRec($User->id);

            return \Helper::jsonResponse(true, "Password changed successfully.", 200);
        } else {
            return \Helper::jsonResponse(false, "Password update failed.", 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        $phone = $request->phone_number;
        $password = $request->password;

        $isEmpty = [
            'phone_number' => $phone,
            'password' => $password
        ];

        if(\Helper::checkEmptyFields($isEmpty)) return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty));
        if(\Helper::validatePhone($phone)) return \Helper::validatePhone($phone);

        $formatted = "+234".(int)$phone;
        $User = User::checkIfExists('phone_number', $formatted);
        if(!$User) return \Helper::jsonResponse(false, "Invalid phone number entered.");

        //update credentials
        User::updateRecord($User->id, ['password' => $password, 'is_default_pass_changed' => 'Yes']);
        return \Helper::jsonResponse(true, "Password changed successfully.", 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPasswordResetOTP(Request $request) {
        $otp = $request->otp_code;
        $phone = $request->phone;

        if(!$otp){return \Helper::jsonResponse(false, 'The OTP field is required.', 400);}

        $UserCode = UserCode::where('otp', $otp)
            ->latest()
            ->first();
        $User = User::where('id', $UserCode->user_id)->first();
        $User_phone = $User['phone_number'];
        $User_email = $User['email'];

        if(!$UserCode){return \Helper::jsonResponse(false, 'Invalid OTP sent!', 400);}
        if((int)$UserCode->is_used){return \Helper::jsonResponse(false, 'OTP has already been used.', 400);}

        //check if OTP has expired after 30 seconds
        $updated_at = Carbon::make($UserCode->updated_at);
        if(\Helper::validateOTP($updated_at))return \Helper::jsonResponse(false, 'OTP already expired, click on Resend Code',400);

        //update the user otp , create an access token and get the user data
        $UserCode->is_used = 1;
        $UserCode->save();

        return \Helper::jsonResponse(
            true,
            "Password Reset OTP verified!.",
            200,
            [
                'status' => true,
                'phone_number' => $User_phone,
                'email' => $User_email,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetUserPassword(Request $request){
        $phone = $request->phone_number;
        $email = $request->email;
        $password = $request->password;
        $isEmpty = ['password'=>$password];

        // Validate inputs
        if(\Helper::checkEmptyFields($isEmpty)) return \Helper::jsonResponse(false, \Helper::checkEmptyFields($isEmpty), 400);
        if($phone) {
            $User = User::checkIfExists('phone_number', $phone);
            if(!$User) return Helper::jsonResponse(false, "Invalid phone number entered.", 400);
        }

        if($email) {
            $User = User::checkIfExists('email', $email);
            if(!$User) return Helper::jsonResponse(false, "Invalid email address entered.", 400);
        }
        //update credentials
        User::updateRecord(
            $User->id, [
                'password'=>$password,
                'is_default_pass_changed'=>'Yes'
            ]
        );
        // Send Password reset successful email
//        $email_address = $User->email;
//        $recipient_name = $User->name;
//        $d = ['recipient_name' => $recipient_name];
//        $configData = [
//            'sender_email'=> Helper::config('site_email'),
//            'sender_name'=> Helper::config('site_name'),
//            'recipient_email'=> $email_address,
//            'recipient_name'=> $recipient_name,
//            'subject'=> 'Password Reset'
//        ];
//
//        EmailController::dispatchMail($configData, 'emails.password_reset', $d);

        return \Helper::jsonResponse(true, "Password changed successfully.", 200);
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required',
            'phone_number' => 'required',
//                    'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|confirmed'
        ]);

        if($validator->fails()){
//            return $this->sendError('Validation Error.', $validator->errors());
            return back()->with('error', $validator->errors());
        }

        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $phone_number = $request->phone_number;
        $farm_state_id = 1; // $request->farm_state_id;
        $role_id = 1;
        $password = $request->password;
        $referral_code = $request->referral_code;
        $email = $request->email;
        if(is_numeric($phone_number)){
            if(\Helper::validatePhone($phone_number)) return \Helper::validatePhone($phone_number);
            //format the phone number
            $phone_number = '+234'.(int)$phone_number;
            $column = 'phone_number';
        }
        $newAccount = self::createNewUserWithPhone(
            $phone_number,
            'dummy.jpg',
            $first_name,
            $last_name,
            $farm_state_id,
            $role_id,
            $password,
            $email
        );
        if($newAccount) return redirect('/login')->with('status', 'Account created successfully.');
        else return back()->with('error', 'Failed to create Account.');


    }
}

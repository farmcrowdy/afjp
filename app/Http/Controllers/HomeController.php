<?php

namespace App\Http\Controllers;

use App\Models\Farmer;
use App\Models\FarmerLoan;
use App\Models\FarmerLoanLoanBundle;
use App\Models\LoanBundle;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function update_profile(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'farm_state_id' => 'required',
            'phone_number' => 'required',
//                    'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $phone_number = $request->phone_number;
        if(is_numeric($phone_number)) {
            if(\Helper::validatePhone($phone_number)) return \Helper::validatePhone($phone_number);
            //format the phone number
            $phone_number = '+234'.(int)$phone_number;
            $column = 'phone_number';
        }
        $user = User::where('id', \auth()->id())->first();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->farm_state_id = $request->farm_state_id;
        $user->phone_number = $phone_number;
        $user->save();

        if($user) {
            return \Helper::jsonResponse(true, 'Updated User Information successfully.', 200, $user);
        } else {
            return \Helper::jsonResponse(false, 'Failed to update User Information.', 400);
        }

    }

    public function user_profile() {
        $user = auth()->user();
        $user->avatar = asset('storage/images/avatar/' . $user->avatar);
        if($user) {
            return \Helper::jsonResponse(true,'User Profile retrieved successfully.', 200, $user);
        } else {
            return \Helper::jsonResponse(false, 'User record not found', 400);
        }
    }

    public function uploadAvatar(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $imageName = 'user_'.time().'.'.request()->avatar->getClientOriginalExtension();
        $request->avatar->storeAs('public/images/avatar/', $imageName);
        $user = User::find(auth()->user()->id);
        $user->avatar = $imageName;
        $user->save();

        if($user) {
            return \Helper::jsonResponse(true, 'Uploaded Profile Image successfully.', 200, $user);
        } else {
            return \Helper::jsonResponse(false, 'Failed to upload Profile Image.');
        }


    }

    public function getAvatar() {
        $data['avatar'] = asset('storage/images/avatar/'.auth()->user()->avatar);
        if($data) {
            return \Helper::jsonResponse(true, 'Fetched User avatar', 200, $data);
        } else {
            return \Helper::jsonResponse(false, 'User Avatar not found.', 400);
        }
    }

    public function dashboardSummary() {
        $user = \auth()->user();
        $loansDisbursed = FarmerLoan::where('user_id', \auth()->id())->where('status_id', 3)->sum('amount');
        $loansDisbursedToday = FarmerLoan::where('user_id', \auth()->id())->where('status_id', 3)->whereDate('created_at', Carbon::today())->sum('amount');

        $data = array(
            'loansDisbursed' => $loansDisbursed > 0 ? $loansDisbursed : 0,
            'loansDisbursedToday' => $loansDisbursedToday > 0 ? $loansDisbursedToday : 0,
            'user' => $user
        );

        return \Helper::jsonResponse(true, 'Dashboard Summary',200, $data);
    }

    public function farmersDisbursedToday() {
        $loansDisbursedToday = FarmerLoan::where('user_id', \auth()->id())->whereDate('created_at', Carbon::today())->get();
        if(sizeof($loansDisbursedToday) > 0) {
            foreach ($loansDisbursedToday as $ldt) {
                $ldt->farmer = Farmer::where('id', $ldt->farmer_id)->select('First_Name', 'Last_Name', 'Phone', 'Crop')->first();
                $loanBundleIds = FarmerLoanLoanBundle::where('farmer_loan_id', $ldt->id)->distinct()->select('loan_bundle_id')->get();
                $loanBundles = [];
                foreach($loanBundleIds as $loanBundleId) {
                    array_push($loanBundles, LoanBundle::where('id', $loanBundleId->loan_bundle_id)->select('name')->get()[0]->name);
                }
                $ldt->loan_bundles = $loanBundles;
            }
            return \Helper::jsonResponse(true, 'Farmers Disbursed Today', 200, $loansDisbursedToday);
        } else {
            return \Helper::jsonResponse(false, 'No Loans disbursed today.', 400);
        }
    }

    public function farmersDisbursed() {
        $loansDisbursedToday = FarmerLoan::where('user_id', \auth()->id())->whereDate('created_at', '!=', Carbon::today())->get();
        if(sizeof($loansDisbursedToday) > 0) {
            foreach ($loansDisbursedToday as $ldt) {
                $ldt->farmer = Farmer::where('id', $ldt->farmer_id)->select('First_Name', 'Last_Name', 'Phone', 'Crop')->first();
                $loanBundleIds = FarmerLoanLoanBundle::where('farmer_loan_id', $ldt->id)->distinct()->select('loan_bundle_id')->get();
                $loanBundles = [];
                foreach($loanBundleIds as $loanBundleId) {
                    array_push($loanBundles, LoanBundle::where('id', $loanBundleId->loan_bundle_id)->select('name')->get()[0]->name);
                }
                $ldt->loan_bundles = $loanBundles;
            }
            return \Helper::jsonResponse(true, 'Farmers Disbursed', 200, $loansDisbursedToday);
        } else {
            return \Helper::jsonResponse(false, 'No Loans disbursed.', 400);
        }
    }

    public function dashboard() {
        $user = \auth()->user();
         $loansDisbursed = FarmerLoan::where('user_id', \auth()->id())->where('status_id', 3)->get();
         $loansDisbursedSum = FarmerLoan::where('user_id', \auth()->id())->where('status_id', 3)->sum('amount');
        $loansDisbursedToday = FarmerLoan::where('user_id', \auth()->id())->where('status_id', 3)->whereDate('created_at', Carbon::today())->get();
        $loansDisbursedTodaySum = FarmerLoan::where('user_id', \auth()->id())->where('status_id', 3)->whereDate('created_at', Carbon::today())->sum('amount');

        $data = array(
            'loansDisbursedSum' => $loansDisbursedSum > 0 ? $loansDisbursedSum : 0,
            'loansDisbursed' => $loansDisbursed,
            'loanDisbursedTodaySum' => $loansDisbursedTodaySum > 0 ? $loansDisbursedTodaySum : 0,
            'loanDisbursedToday' => $loansDisbursedToday,
            'user' => $user
        );

        return \Helper::jsonResponse(true, 'Dashboard Summary',200, $data);
    }

    public function getLoanBundles($loansDisbursedLoanBundles) {
        dd($loansDisbursedLoanBundles);
    }
}

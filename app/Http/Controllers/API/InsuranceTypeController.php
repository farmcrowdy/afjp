<?php

namespace App\Http\Controllers\API;

use App\Models\Farmer;
use App\Models\InsuranceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InsuranceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insurance_types = InsuranceType::where('status_id', 1)->get();
        if(sizeof($insurance_types) > 0) {
            $data = [
                'insurance_packages' => $insurance_types,
                'count' => sizeof($insurance_types)
            ];
            return \Helper::jsonResponse(true, 'Retrieved Insurance Packages', 200, $data);
        } else {
            return \Helper::jsonResponse(false, 'Insurance Packages not available.', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'phone_number' => 'required|numeric',
            'insurance_type_id' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $farmer = Farmer::where('phone_number', $request->phone_number)->first();
        if(!is_null($farmer)) {

        } else {
            return \Helper::jsonResponse(false, 'Farmer not found.', 400);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function show(int $insuranceTypeID)
    {
        $insuranceType = InsuranceType::find($insuranceTypeID);
        if(!is_null($insuranceType)) {
            return \Helper::jsonResponse(true, 'Retrieved Insurance Package with ID '.$insuranceTypeID, 200, $insuranceType);
        } else {
            return \Helper::jsonResponse(false, 'Failed to fetch Insurance Package with ID '.$insuranceTypeID, 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function edit(InsuranceType $insuranceType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsuranceType $insuranceType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsuranceType $insuranceType)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Models\Input;
use App\Models\LoanBundle;
use App\Models\LoanBundleInputs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoanBundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loanBundles = LoanBundle::paginate(100);
        $loanBundles = $this->getMultipleLoanBundlesPrices($loanBundles);
        if($loanBundles) {
            return \Helper::jsonResponse(true, 'Retrieved Loan Bundles successfully.', 200, $loanBundles);
        } else {
            return \Helper::jsonResponse(false, 'No Loan Bundles available.', 400);
        }
    }

    public function getMultipleLoanBundlesPrices($loanBundles) {
        foreach ($loanBundles as $loanBundle) {
            $loanBundle->price = $this->getSingleLoanBundlePrice($loanBundle);
        }
        return $loanBundles;
    }

    public function getSingleLoanBundlePrice($loanBundle) {
        $amount = 0;
        $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $loanBundle->id)->get();
        foreach ($loanBundleInputs as $loanBundleInput) {
            $amount += Input::find($loanBundleInput->input_id)->price * $loanBundleInput->unit;
        }
        return $amount;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $loanBundle = LoanBundle::create($input);
        if($loanBundle) {
            return \Helper::jsonResponse(true, 'Created Loand Bundle successfully.', 200, $loanBundle);
        } else {
            return \Helper::jsonResponse(false, 'Failed to create Loan Bundle', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoanBundle  $loanBundle
     * @return \Illuminate\Http\Response
     */
    public function show(int $loanBundleId)
    {
        $loanBundle = LoanBundle::find($loanBundleId);
        if($loanBundle) {
            return \Helper::jsonResponse(true, 'Retrieved Loan Bundle successfully.', 200, $loanBundle);
        } else {
            return \Helper::jsonResponse(false, 'Loan Bundle does not exist.', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LoanBundle  $loanBundle
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanBundle $loanBundle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LoanBundle  $loanBundle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $loanBundleId)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $lb = LoanBundle::where('id', $loanBundleId)->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'status_id' => $request->status_id
        ]);

        if($lb) {
            return \Helper::jsonResponse(true, 'Updated Loan Bundle successfully.', 200, $lb);
        } else {
            return \Helper::jsonResponse(false, 'Failed to create Loan Bundle.', 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoanBundle  $loanBundle
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $loanBundleId)
    {
        $lb = LoanBundle::where('id', $loanBundleId)->delete();
        if($lb) {
            return \Helper::jsonResponse(true, 'Deleted Loan Bundle Successfully.', 200, $lb);
        } else {
            return \Helper::jsonResponse(false, 'Failed to delete Loan Bundle', 400);
        }
    }
}

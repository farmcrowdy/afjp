<?php

namespace App\Http\Controllers\API;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = Status::all();
        return response()->json($statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $status = new Status();
        $status->name = $request->name;
        $status->description = $request->description;
        $status->save();

        if($status) {
            return \Helper::jsonResponse(true, 'Created Status record successfully.', 200, $status);
        } else {
            return \Helper::jsonResponse(false, 'Failed to create status record', 400);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show(int $status_id)
    {
        $status = Status::where('id', $status_id)->first();
        if(!is_null($status)) {
            return \Helper::jsonResponse(true, 'Retrieved Status record successfully', 200, $status);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Status record', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit(Status $status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $status_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $status = Status::where('id', $status_id)->first();
        $status->name = $request->name;
        $status->description = $request->description;
        $status->save();

        if($status) {
            return \Helper::jsonResponse(true, 'Updated Status record successfully', 200, $status);
        } else { return \Helper::jsonResponse(false, 'Failed to update Status record', 400); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $status_id)
    {
        $status = Status::where('id', $status_id)->delete();
        if($status) {
            return \Helper::jsonResponse(true, 'Deleted status record successfully', 200);
        } else {
            return \Helper::jsonResponse(false, 'Failed to delete Status record', 400);
        }
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Models\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configs = Config::all();

        if(sizeof($configs) > 0) {
            return \Helper::jsonResponse(true, 'Retrieved configs successfully.', 200, $configs);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve configs', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'value' => 'required',
            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $config = Config::create($input);
        if($config) {
            return \Helper::jsonResponse(true, 'Created Config record successfully.', 200, $config);
        } else { return \Helper::jsonResponse(false, 'Failed to create config record', 400); }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function show(int $config)
    {
        $config = Config::where('id', $config)->first();
        if($config) {
            return \Helper::jsonResponse(true, 'Retrieve Config record successfully.', 200, $config);
        } else {return \Helper::jsonResponse(false, 'Failed to retrieve config record', 400);}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function edit(Config $config)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $config_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'value' => 'required',
            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $config = Config::where('id', $config_id)->first();
        $config->name = $request->name;
        $config->value = $request->value;
        $config->status_id = $request->status_id;
        $config->save();

        if($config) {
            return \Helper::jsonResponse(true, 'Updated config record successfully.', 200, $config);
        } else {
            return \Helper::jsonResponse(false, 'Failed to update config record', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $config)
    {
        $config = Config::where('id', $config)->delete();
        if($config) {
            return \Helper::jsonResponse(true, 'Deleted Config record successfully.', 200, $config);
        } else { return \Helper::jsonResponse(false, 'Failed to delete Config record.', 400); }
    }
}

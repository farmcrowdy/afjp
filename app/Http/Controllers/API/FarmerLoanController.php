<?php

namespace App\Http\Controllers\API;

use App\Models\Farmer;
use App\Models\FarmerLoan;
use App\Models\FarmerLoanLoanBundle;
use App\Models\Input;
use App\Models\LoanBundle;
use App\Models\LoanBundleInputs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Web\FarmerLoanController as FLC;
use App\Http\Controllers\API\LoanBundleController as LBC;

class FarmerLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmerloans = FarmerLoan::where('status_id', 3)->paginate(10000);
        if(sizeof($farmerloans) > 0) {
            $total = 0;
            foreach ($farmerloans as $fls) {
                $total += $fls->amount;
            }
            $data = array(
                'total' => $total,
                'loans' => $farmerloans
            );
            return \Helper::jsonResponse(true, 'Retrieved All Loan records. ', 200, $data);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Loan records.', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'farmer_id' => 'required',
            'loan_bundles' => 'required'
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error. ', $validator->errors());
        }
        $loan_bundle_json = json_decode($request->loan_bundles);
        $amount = $this->getLoanAmount($loan_bundle_json);
        $farmerloan = FarmerLoan::create([
                'farmer_id' => $request->farmer_id,
                'amount' => $amount,
                'status_id' => 3,
                'user_id' => \auth()->id()
            ]);
        if($farmerloan) {
            $flc = new FLC();
            $cfllb = $flc->createFarmerLoanLoanBundles($loan_bundle_json, $farmerloan->id);
            if($cfllb) return \Helper::jsonResponse(true, 'Created Farmer Loan record successfully. Loan Bundle records also created.', 200, $farmerloan);
            else return \Helper::jsonResponse(false, 'Created Farmer Loan record successfully. Failed to create Loan Bundle records, please contact Admin.', 400);
        } else {
            return \Helper::jsonResponse(false, 'Failed to create Farmer Loan record.', 400);
        }

    }

    /**
     * @param $loan_bundle_json
     * @return float|int
     */
    public function getLoanAmount($loan_bundle_json) {
        $amount = 0;
        foreach($loan_bundle_json as $loan_bundle_id) {
            $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $loan_bundle_id)->get();
            foreach ($loanBundleInputs as $loanBundleInput) {
                $input = Input::find($loanBundleInput->input_id);
                $amount += $input->price * $loanBundleInput->unit;
            }
        }
        return $amount;
    }

    /**
     * Display the specified resource.
     *
     * @param int $farmer_loan_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $farmer_loan_id)
    {
        $farmer_loan = FarmerLoan::find($farmer_loan_id);
        if(!is_null($farmer_loan)) {
            return \Helper::jsonResponse(true, 'Retrieved Farmer Loan record.', 200, $farmer_loan);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Farmer Loan record.', 400);
        }
    }

    /**
     * @param int $farmer_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showFarmerLoan(int $farmer_id)
    {
        $farmer_loans = FarmerLoan::where('farmer_id', $farmer_id)->get();
        if(!is_null($farmer_loans)) {
            $total = 0;
            $loan_bundle_ids = [];
            foreach ($farmer_loans as $farmer_loan) {
                $total += $farmer_loan->amount;
                $farmerLoanLoanBundles = $this->farmerLoanLoanBundles($farmer_loan);
//                $farmer_loan->loan_bundle = $farmerLoanLoanBundles;
                $farmer_loan->loan_bundle = $this->fix_object($farmerLoanLoanBundles);
            }
            $farmer = Farmer::where('id', $farmer_id)->select('First_Name', 'Last_Name', 'Crop', 'Phone')->first();
            $data = array(
                'farmer' => $farmer,
                'loan_records' => $farmer_loans,
                'total' => $total
            );
            return \Helper::jsonResponse(true, 'Retrieved Farmer Loan records.', 200, $data);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Farmer Loan record.', 400);
        }
    }

    /**
     * @param $object
     * @return mixed
     */
    function fix_object( $object ) {
        $dump = serialize( $object );
        $dump = str_replace('s:6:"series";r:1;','s:6:"series";s:6:"series";',$dump);
        $dump = preg_replace( '/^O:\d+:"[^"]++"/', 'O:8:"stdClass"', $dump );
        $dump = preg_replace_callback( '/:\d+:"\0.*?\0([^"]+)"/', function($matches){
            return ":" . strlen( $matches[1] ) . ":\"" . $matches[1] . "\"";}, $dump );
        return unserialize( $dump );
    }

    /**
     * @param $farmer_loan
     * @return mixed
     */
    public function farmerLoanLoanBundles($farmer_loan) {
        $loan_bundle_ids = FarmerLoanLoanBundle::where('farmer_loan_id', $farmer_loan->id)->pluck('loan_bundle_id');
        $loan_bundles = LoanBundle::find($loan_bundle_ids);
        $farmer_loan->loan_bundles = $this->computeLoanBundlePrice($loan_bundles);
//        $farmer_loan->loan_bundles = $loan_bundles;
        return $farmer_loan;
    }

    /**
     * @param $loanBundles
     * @return mixed
     */
    public function computeLoanBundlePrice($loanBundles) {
        $price = 0;
        foreach ($loanBundles as $loanBundle) {
            $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $loanBundle->id)->select('input_id', 'unit')->get();
            foreach ($loanBundleInputs as $loanBundleInput) {
                $input = Input::find($loanBundleInput->input_id);
                $price += $input->price * $loanBundleInput->unit;
            }
            $loanBundle->price = $price;
        }
        return $loanBundles;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FarmerLoan  $farmerLoan
     * @return \Illuminate\Http\Response
     */
    public function edit(FarmerLoan $farmerLoan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FarmerLoan  $farmerLoan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $farmer_loan_id)
    {
        $farmer_loan = FarmerLoan::where('id', $farmer_loan_id)->first();
        $farmer_loan->farmer_id = $request->farmer_id;
        $farmer_loan->loan_bundle_id = $request->amount;
        $farmer_loan->status_id = $request->status_id;
        $farmer_loan->save();
        if($farmer_loan) {
            return \Helper::jsonResponse(true, 'Updated Farmer Loan record', 200, $farmer_loan);
        } else {
            return \Helper::jsonResponse(false, 'Failed to update Farmer Loan record', '400');
        }
    }

    public function repayLoan(Request $request, $id) {
        $repay = FarmerLoan::where('id', $id)->update(
            [
                'status_id' => 4,
                'repayment_date' => Carbon::now(),
                'approved_by_agent_id' => $request->user_id
                ]
        );

        if($repay) {
            return \Helper::jsonResponse(true, 'Loan Status changed to Repaid.', 200, ['repay' => $repay]);
        } else {
            return \Helper::jsonResponse(false, 'Failed to change Loan status', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FarmerLoan  $farmerLoan
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $farmerLoan_id)
    {
        $fl = FarmerLoan::where('id', $farmerLoan_id)->delete();
        if($fl) {
            return \Helper::jsonResponse(true, 'Deleted Farmer Loan record successfully.', 200, $fl);
        } else {
            return \Helper::jsonResponse(false, 'Failed to delete Farmer Loan record', 400);
        }
    }
}

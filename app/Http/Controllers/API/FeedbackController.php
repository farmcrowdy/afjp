<?php

namespace App\Http\Controllers\API;

use App\Models\Farmer;
use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fbs = Feedback::where('status_id', 1)->get();
        if (sizeof($fbs) > 0) {
            return \Helper::jsonResponse(true, 'Feedbacks retrieved successfully.', 200, $fbs);
        } else {
            return \Helper::jsonResponse(false, 'Feedbacks not available.', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'farmer_phone' => 'required',
            'feedback_text' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $farmer = Farmer::where('Phone', $request->farmer_phone)->first();
        if($farmer) {
            $farmer_id = $farmer->id;
            $feedback = Feedback::create([
                'status_id' => 1,
                'farmer_id' => $farmer_id,
                'user_id' => \auth()->id(),
                'feedback_text' => $request->feedback_text,
            ]);
            if($feedback) {
                return \Helper::jsonResponse(true, 'Feedback created successfully', 200, $feedback);
            } else {
                return \Helper::jsonResponse(false, 'Failed to create Feedback, check input and try again.', 400);
            }
        } else {
            return \Helper::jsonResponse(false, 'Farmer does not exist in our Database', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(int $feedback_id)
    {
        $feedback = Feedback::find($feedback_id);
        if(!is_null($feedback)) {
            return \Helper::jsonResponse(true, 'Retrieved Feedback successfully.', 200, $feedback);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Feedback with the given ID', 400);
        }
    }


    /**
     * @param int $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUserFeedback(int $user_id)
    {
        $feedbacks = Feedback::where('user_id', $user_id)->get();
        if($feedbacks) {
            foreach ($feedbacks as $feedback) {
                $farmer = Farmer::where('id', $feedback->farmer_id)->first();
                $feedback->farmer_phone = $farmer->Phone;
            }
            return \Helper::jsonResponse(true, 'Retrieved Feedbacks successfully.', 200, $feedbacks);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Feedback with the given ID', 400);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $feedback_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'farmer_phone' => 'required',
            'feedback_text' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $farmer = Farmer::where('Phone', $request->farmer_phone)->first();
        $feedback = Feedback::find($feedback_id);

        if($feedback) {
            $feedback->feedback_text = $request->feedback_text;
            $feedback->farmer_id = $farmer->id;
            $feedback->save();

            return \Helper::jsonResponse(true, 'Feedback updated successfully.', 200, $feedback);
        } else {
            return \Helper::jsonResponse(false, 'Failed to update feedback', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $feedback_id)
    {
        $feedback = Feedback::where('id', $feedback_id)->delete();

        if($feedback) {
            return \Helper::jsonResponse(true, 'Deleted Feedback successfully', 200, $feedback);
        } else {
            return \Helper::jsonResponse(false, 'failed to delete Feedback', 4);
        }
    }
}

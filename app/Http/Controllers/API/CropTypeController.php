<?php

namespace App\Http\Controllers\API;

use App\Models\CropType;
use App\Models\CropTypeLoanBundle;
use App\Models\Input;
use App\Models\LoanBundle;
use App\Models\LoanBundleInputs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helper;

class CropTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $croptypes = CropType::where('status_id', 1)->paginate(100);

        if(sizeof($croptypes) > 0) {
            foreach($croptypes as $ct) {
                $ct['image'] = asset('storage/images/crop_types/' . $ct['image']);
                $ctlbs = CropTypeLoanBundle::where('crop_type_id', $ct['id'])->get();
                if($ctlbs) {
                    $loan_bundles = [];
                    foreach ($ctlbs as $ctlb) {
                        $ctlb = LoanBundle::find($ctlb->loan_bundle_id);
                        $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $ctlb->id)->get();
                        $amount = 0;
                        foreach ($loanBundleInputs as $loanBundleInput) {
                            $amount += Input::find($loanBundleInput->input_id)->price * $loanBundleInput->unit;
                        }
                        $ctlb->price = $amount;
//                        $ctlb['amount'] = \Helper::loan_bundle_input_price($ctlb['id']);
                        array_push($loan_bundles, $ctlb);
                    }
                    $ct['loan_bundle'] = $loan_bundles;
                    $ct['size'] = sizeof($loan_bundles);
                } else {
                    $ct['loan_bundle'] = [];
                    $ct['size'] = 0;
                }
            }
            return \Helper::jsonResponse(true, 'Retrieved Crop Types successfully.', 200, $croptypes);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Crop Types', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $imageName = 'crop_type_image_'.time().'.'.request()->image->getClientOriginalExtension();
        $request->image->storeAs('public/images/crop_types', $imageName);

        $croptype = CropType::create($input);
        $croptype->image = $imageName;
        $croptype->save();
        if($croptype) {
            return \Helper::jsonResponse(true, 'Stored Crop Type successfully.', 200, $croptype);
        } else {
            return \Helper::jsonResponse(false, 'Failed to store Crop Type', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CropType  $cropType
     * @return \Illuminate\Http\Response
     */
    public function show($crop_type)
    {
        $cropType = CropType::where('name', $crop_type)->orWhere('id', $crop_type)->first();
        if($cropType) {
            $cropType->image = asset('storage/images/crop_types/' . $cropType->image);

            $ctlbs = CropTypeLoanBundle::where('crop_type_id', $cropType->id)->get();
            $loan_bundles = [];
            if($ctlbs) {
                foreach ($ctlbs as $ctlb) {
                    $ctlb = LoanBundle::find($ctlb->loan_bundle_id);
                    $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $ctlb->id)->get();
                    $amount = 0;
                    foreach ($loanBundleInputs as $loanBundleInput) {
                        $amount += Input::find($loanBundleInput->input_id)->price * $loanBundleInput->unit;
                    }
                    $ctlb->price = $amount;
                    array_push($loan_bundles, $ctlb);
                }
                $cropType->loan_bundles = $loan_bundles;
                $cropType->size_of_loan_bundles = sizeof($loan_bundles);
            } else {
                $cropType->loan_bundles = [];
                $cropType->size_of_loan_bundles = 0;
            }

            return \Helper::jsonResponse(true, 'Retrieved Crop Type record successfully. ', 200, $cropType);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Crop Type record', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CropType  $cropType
     * @return \Illuminate\Http\Response
     */
    public function edit(CropType $cropType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CropType  $cropType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $cropType)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $cropType = CropType::where('id', $cropType)->first();
        if($cropType) {
            $imageName = 'crop_type_image_'.time().'.'.request()->image->getClientOriginalExtension();
            $request->image->storeAs('images/crop_types', $imageName);

            $cropType->name = $request->name;
            $cropType->description = $request->description;
            $cropType->image = $imageName;
            $cropType->status_id = $request->status_id;
            $cropType->save();

            if($cropType) {
                return \Helper::jsonResponse(true, 'Updated Crop Type record successfully.', 200, $cropType);
            } else {
                return \Helper::jsonResponse(false, 'Failed to update Crop Type record.', 400);
            }
        } else {
            return \Helper::jsonResponse(false, 'Crop Type does not exist.', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CropType  $cropType
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $cropType)
    {
        $cT = CropType::where('id', $cropType)->delete();
        if($cT) {
            return \Helper::jsonResponse(true, 'Deleted Crop Type record successfully.', 200, $cT);
        } else {
            return \Helper::jsonResponse(false, 'Failed to delete Crop Type record', 400);
        }
    }
}

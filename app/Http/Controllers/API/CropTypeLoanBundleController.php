<?php

namespace App\Http\Controllers\API;

use App\Models\CropTypeLoanBundle;
use App\Models\CropType;
use App\Models\LoanBundle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CropTypeLoanBundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $cropType)
    {
        $ctlbs = CropTypeLoanBundle::where('crop_type_id', $cropType)->get();
        if($ctlbs) {
            $loan_bundles = [];
            foreach ($ctlbs as $ctlb) {
                $ctlb = LoanBundle::find($ctlb->loan_bundle_id);
                array_push($loan_bundles, $ctlb);
            }
            if(sizeof($loan_bundles) > 0) {
                $data['loan_bundle'] = $loan_bundles;
                $data['size'] = sizeof($loan_bundles);
                return \Helper::jsonResponse(true, 'Crop Type Loan Bundle(s)', 200, $data);
            } else {
                $data['loan_bundle'] = 'No related Loan Bundles';
                $data['size'] = 0;
                return \Helper::jsonResponse(true, 'Crop Type Loan Bundle(s)', 200, $data);
            }
        } else {
            return \Helper::jsonResponse(false, 'No Loan Bundle(s) available for this Crop Type', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CropTypeLoanBundle  $cropTypeLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function show(CropTypeLoanBundle $cropTypeLoanBundle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CropTypeLoanBundle  $cropTypeLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function edit(CropTypeLoanBundle $cropTypeLoanBundle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CropTypeLoanBundle  $cropTypeLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CropTypeLoanBundle $cropTypeLoanBundle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CropTypeLoanBundle  $cropTypeLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function destroy(CropTypeLoanBundle $cropTypeLoanBundle)
    {
        //
    }
}

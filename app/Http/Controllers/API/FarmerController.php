<?php

namespace App\Http\Controllers\API;

use App\Models\Farmer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FarmerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = DB::table('farmers')->paginate(100);
        if($farmers) {
            return \Helper::jsonResponse(true, 'List of all Farmers retrieved', 200, $farmers);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Farmers list', 400);
        }
    }

    public function farmersDistribution() {
        $farmers = DB::table('farmers')
            ->select(DB::raw('count(farmers.State) as count, states.id, states.name, farmers.State'))
            ->leftJoin('states','states.id', 'farmers.State')
            ->groupBy('farmers.State')
            ->orderBy('farmers.State')
            ->get();
        if($farmers) {
            return \Helper::jsonResponse(true, 'List of all Farmers retrieved', 200, $farmers);
        } else {
            return \Helper::jsonResponse(false, 'Failed to retrieve Farmers list', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Farmer  $farmer
     * @return \Illuminate\Http\Response
     */
    public function show($parame)
    {
        $farmer = '';
        if(strlen($parame) == 11) {
            $farmer = Farmer::where('Phone', $parame)->first();
        } elseif(is_numeric($parame)) {
            $farmer = Farmer::where('id', $parame)->first();
        }

        if(!is_null($farmer)) {
            return \Helper::jsonResponse(true, 'Retrieved Farmer details', 200, $farmer);
        } else { return \Helper::jsonResponse(false, 'Farmer not found!.', 400); }
    }

    public function confirmPin($farmer_id, $pin) {
        $farmer = Farmer::find($farmer_id);
        if($farmer) {
            if($farmer->pin == $pin) {
                return \Helper::jsonResponse(true, 'PIN confirmed successfully.', 200, $farmer);
            } else {
                return \Helper::jsonResponse(false, 'Incorrect PIN entered.', 400);
            }
        } else {
            return \Helper::jsonResponse(false, 'Farmer Information not correct.', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Farmer  $farmer
     * @return \Illuminate\Http\Response
     */
    public function edit(Farmer $farmer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Farmer  $farmer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Farmer $farmer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Farmer  $farmer
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $farmer)
    {
        $farmer = Farmer::where('id', $farmer)->delete();
        if($farmer) {
            return \Helper::jsonResponse(true, 'Deleted User record successfully.', 200, $farmer);
        } else {
            return \Helper::jsonResponse(false, 'Failed to delete User record', 400);
        }
    }
}

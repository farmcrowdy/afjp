<?php

namespace App\Http\Controllers\API;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $role = Role::create([
           'name' => $request->name,
           'description' => $request->description
        ]);

        if($role) {
            return redirect('admin/role/all')->with('status', 'Created Role successfully.');
        } else {
            return back()->with('error', 'Failed to create Role');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(int $role_id)
    {
        $role = Role::find($role_id);
        if($role) {
            return view('admin.role.edit', compact('role'));
        } else {
            return redirect('admin/role/all')->with('error', 'Role record does not exist.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $role_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $role = Role::where('id', $role_id)->update([
           'name' => $request->name,
           'description' => $request->description
        ]);

        if($role) {
            return redirect('admin/role/all')->with('status', 'Updated Role record successfully.');
        } else {
            return back()->with('error', 'Failed to update Role record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $role_id
     * @return void
     */
    public function destroy(int $role_id)
    {
        $role = Role::where('id', $role_id)->delete();
        if($role) {
            return redirect('admin/role/all')->with('status', 'Deleted Role record successfully.');
        } else {
            return back()->with('error', 'Error deleting Role record.');
        }
    }
}

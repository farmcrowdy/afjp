<?php

namespace App\Http\Controllers;

use App\Models\FarmerLoanLoanBundle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FarmerLoanLoanBundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FarmerLoanLoanBundle  $farmerLoanLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function show(FarmerLoanLoanBundle $farmerLoanLoanBundle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FarmerLoanLoanBundle  $farmerLoanLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function edit(FarmerLoanLoanBundle $farmerLoanLoanBundle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FarmerLoanLoanBundle  $farmerLoanLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FarmerLoanLoanBundle $farmerLoanLoanBundle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FarmerLoanLoanBundle  $farmerLoanLoanBundle
     * @return \Illuminate\Http\Response
     */
    public function destroy(FarmerLoanLoanBundle $farmerLoanLoanBundle)
    {
        //
    }
}

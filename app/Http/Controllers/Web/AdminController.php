<?php

namespace App\Http\Controllers\Web;

use App\Models\Admin;
use App\Models\Farmer;
use App\Models\FarmerLoan;
use App\Models\Input;
use App\Models\Role;
use App\Models\State;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = Farmer::count();
        $loanDisbursed = FarmerLoan::sum('amount');
        $agents = User::where('role_id', 2)->count();
        $inputs = Input::count();
        $input_list = Input::all();
        $states = State::all();
        return view('admin.index', compact('farmers', 'loanDisbursed', 'agents', 'inputs', 'states', 'input_list'));
    }

    public function allusers(){
        $users = User::paginate();
//        dd($users);
        return view('admin.user.index', compact('users'));
//        dd($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('admin.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users',
            'phone_number' => 'required|unique:users',
            'role_id' => 'required',
//                    'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|confirmed'
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::create(
            [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'role_id' => $request->role_id,
                'password' => User::protectPassword($request->password)
            ]
        );

        if($user) {
            return redirect('admin/user/all')->with('status', 'User record created successfully');
        } else {
            return back()->with('error', 'Failed to create User record');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(int $user_id)
    {
        $user = User::find($user_id);
        $roles = Role::all();

        return view('admin.user.edit', compact('user', 'roles'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $user_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $user_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'role_id' => 'required'
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::where('id', $user_id)->update(
            [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'role_id' => $request->role_id
            ]
        );

        if($user) {
            return redirect('admin/user/all')->with('status', 'User record edited successfully');
        } else {
            return back()->with('error', 'Failed to update User record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $user_id)
    {
        $user = User::where('id', $user_id)->delete();
        if($user) {
            return redirect('admin/user/all')->with('status', 'User record deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete User record.');
        }
    }
}

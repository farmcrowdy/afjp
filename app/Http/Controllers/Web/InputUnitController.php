<?php

namespace App\Http\Controllers\Web;

use App\Models\InputUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InputUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $units = InputUnit::paginate(20);
        if ($request->is('api*')) {
            return \Helper::jsonResponse(true, 'Retrieved the list of Units successfully.', 200, $units);
        } else {
            return view('admin.input.unit.index', compact('units'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.input.unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'notation' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $iu = InputUnit::create([
            'name' => $request->name,
            'notation' => $request->notation,
            'description' => $request->description
        ]);

        if($iu) {
            return redirect('admin/input/unit/all')->with('status', 'Created Input Unit record successfully.');
        } else {
            return back()->with('error', 'Failed to create Input Unit record');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InputUnit  $inputUnit
     * @return \Illuminate\Http\Response
     */
    public function show(int $inputUnit)
    {
        $unit = InputUnit::where('id', $inputUnit)->first();
        if ($unit) return \Helper::jsonResponse(true, 'Fetched Unit details', 200, $unit);
        else return \Helper::jsonResponse(false, 'Unit not found', 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InputUnit  $inputUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(int $input_unit_id)
    {
        $unit = InputUnit::find($input_unit_id);
        if($unit) {
            return view('admin.input.unit.edit', compact('unit'));
        } else {
            return back()->with('error', 'Input Unit not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InputUnit  $inputUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $input_unit_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'notation' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $iu = InputUnit::where('id', $input_unit_id)->update([
            'name' => $request->name,
            'notation' => $request->notation,
            'description' => $request->description
        ]);

        if($iu) {
            return redirect('admin/input/unit/all')->with('status', 'Updated Input Unit record successfully.');
        } else {
            return back()->with('error', 'Failed to update Input Unit record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InputUnit  $inputUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $input_unit_id)
    {
        $iu = InputUnit::where('id', $input_unit_id)->delete();
        if($iu) {
            return redirect('admin/input/unit/all')->with('status', 'Deleted Input Unit record successfully.');
        } else {
            return back()->with('error', 'Failed to delete Input Unit record');
        }
    }
}

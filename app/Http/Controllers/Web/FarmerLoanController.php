<?php

namespace App\Http\Controllers\Web;

use App\Exports\FarmerLoanExport;
use App\Http\Controllers\Controller;
use App\Models\Farmer;
use App\Models\FarmerLoan;
use App\Models\FarmerLoanLoanBundle;
use App\Models\LoanBundle;
use App\Models\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class FarmerLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmerLoans = FarmerLoan::paginate(20);
//        dd(\auth()->user()->role->name);
        return view('admin.farmer.loan.index', compact('farmerLoans'));
    }

    public function exportFarmerLoan() {
        return Excel::download(new FarmerLoanExport, 'FarmerLoansExport.xlsx');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $farmers = Farmer::all();
        $agents = User::where('role_id', 2)->get();
        $statuses = Status::find([3, 4]);
        $loanBundles = LoanBundle::all();

        return view('admin.farmer.loan.create', compact('farmers', 'agents', 'statuses', 'loanBundles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'farmer_id' => 'required',
            'loan_bundles' => 'required',
            'status_id' => 'required',
            'user_id' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $loan_bundles = LoanBundle::find($request->post('loan_bundles'));
        $loan_bundle_ids = [];
        $amount = 0;
        foreach ($loan_bundles as $loan_bundle) {
            $amount += $loan_bundle->price;
            array_push($loan_bundle_ids, $loan_bundle->id);
        }
        $fl = FarmerLoan::create([
            'farmer_id' => $request->farmer_id,
            'user_id' => $request->user_id,
            'status_id' => $request->status_id,
            'amount' => $amount
        ]);
        if($fl) {
            $this->createFarmerLoanLoanBundles($loan_bundle_ids, $fl->id);
            return redirect('admin/farmer/loan/all')->with('status', 'Loan record created successfully.');
        } else {
            return back()->with('error', 'Failed to create Loan record');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $farmerLoan = FarmerLoan::find($id);
        $farmerLoanBundles = FarmerLoanLoanBundle::where('farmer_loan_id', $id)->get();

        $selectedLBIs = [];
        foreach ($farmerLoanBundles as $loanBundleInput) {
            array_push($selectedLBIs, $loanBundleInput->loan_bundle_id);
        }
        $farmers = Farmer::all();
        $agents = User::where('role_id', 2)->get();
        $statuses = Status::find([3, 4]);
        $loanBundles = LoanBundle::all();
        return view('admin.farmer.loan.edit', compact('farmers', 'agents', 'statuses', 'loanBundles', 'farmerLoan', 'selectedLBIs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'farmer_id' => 'required',
            'loan_bundles' => 'required',
            'status_id' => 'required',
            'user_id' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $loanBundles = LoanBundle::find($request->loan_bundles);
        $amount = 0;
        $loan_bundle_ids = [];
        foreach ($loanBundles as $lbs) {
            $amount += $lbs->price;
            array_push($loan_bundle_ids, $lbs->id);
        }
        $farmerLoan = FarmerLoan::where('id', $id)->update([
           'farmer_id' => $request->farmer_id,
            'amount' => $amount,
            'status_id' => $request->status_id
        ]);
        if($farmerLoan) {
            $deleteFarmerLoanLoanBundleId = FarmerLoanLoanBundle::where('farmer_loan_id', $id)->delete();
            $this->createFarmerLoanLoanBundles($loan_bundle_ids, $id);
            return redirect('admin/farmer/loan/all')->with('status', 'Farmer Loan updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Farmer Loan.');
        }
    }

    public function createFarmerLoanLoanBundles(array $loan_bundles, int $loan_id) {
        foreach ($loan_bundles as $rlbs) {
            FarmerLoanLoanBundle::create([
                'farmer_loan_id' => $loan_id,
                'loan_bundle_id' => $rlbs
            ]);
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fl = FarmerLoan::where('id', $id)->delete();
        if ($fl) {
            return redirect('admin/farmer/loan/all')->with('status', 'Loan Record deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Loan Record');
        }
    }
}

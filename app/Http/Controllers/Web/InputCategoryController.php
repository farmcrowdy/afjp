<?php

namespace App\Http\Controllers\Web;

use App\Models\InputCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InputCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index(Request $request)
    {
        $categories = InputCategory::paginate(20);
        if ($request->is('api*')) {
            if($categories) return \Helper::jsonResponse(true, 'Fetched Input Categories', 200, $categories);
            else return \Helper::jsonResponse(false, 'Input Categories not found', 400);
        } else {
            return view('admin.input.category.index', compact('categories'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.input.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $ic = InputCategory::create([
            'name' => $request->name,
            'description' => $request->description
        ]);

        if ($ic) {
            return redirect('admin/input/category/all')->with('status', 'Input Category created Successfully.');
        } else {
            return back()->with('error', 'Failed to create Input Category');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InputCategory  $inputCategory
     * @return \Illuminate\Http\Response
     */
    public function show(InputCategory $inputCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InputCategory  $inputCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(int $input_category_id)
    {
        $inputCategory = InputCategory::where('id', $input_category_id)->first();
        return view('admin.input.category.edit', compact('inputCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InputCategory  $inputCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $input_category_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $ic = InputCategory::where('id', $input_category_id)->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        if ($ic) {
            return redirect('admin/input/category/all')->with('status', 'Input Category updated Successfully.');
        } else {
            return back()->with('error', 'Failed to update Input Category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InputCategory  $inputCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(InputCategory $inputCategory)
    {
        //
    }
}

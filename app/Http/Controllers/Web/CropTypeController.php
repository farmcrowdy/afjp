<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CropType;
use App\Models\CropTypeLoanBundle;
use App\Models\LoanBundle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CropTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $croptypes = CropType::paginate(10);
        foreach ($croptypes as $croptype) {
            $croptype->loan_bundle = CropTypeLoanBundle::where('crop_type_id', $croptype->id)->count();
        }
        return view('admin.croptype.index', compact('croptypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loanBundles = LoanBundle::all();
        return view('admin.croptype.create', compact('loanBundles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
//            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
//            'status_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $imageName = 'crop_type_image_'.time().'.'.request()->image->getClientOriginalExtension();
        $storeImage = $request->image->storeAs('images/crop_types', $imageName);

        $croptype = CropType::create([
           'name' => $request->name,
           'description' => $request->description,
           'status_id' => 1,
           'image' => $imageName,
        ]);

        if($croptype) {
            foreach ($request->bundles as $key => $value) {
                CropTypeLoanBundle::create([
                    'crop_type_id' => $croptype->id,
                    'loan_bundle_id' => $value
                ]);
            }
            return redirect('admin/croptype/all')->with('status', 'Crop Type created successfully.');
        } else {
            return back()->with('error', 'Failed to create Crop Type');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $croptype = CropType::find($id);
        $loanBundles = LoanBundle::all();
        $croptypeloanbundles = CropTypeLoanBundle::where('crop_type_id', $id)->get();
        $bundles = [];
        foreach ($croptypeloanbundles as $ctlb) {
            array_push($bundles, $ctlb->loan_bundle_id);
        }

        return view('admin.croptype.edit', compact('croptype', 'loanBundles', 'croptypeloanbundles', 'bundles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $croptype = CropType::find($id);
        $croptype->name = $request->name;
        $croptype->description = $request->description;
        $croptype->save();
        $deleteLoanBundles = CropTypeLoanBundle::where('crop_type_id', $id)->delete();
        foreach ($request->bundles as $key => $value) {
            $lb = CropTypeLoanBundle::create([
                'crop_type_id' => $id,
                'loan_bundle_id' => $value
            ]);
        }
        if($croptype) {
            return redirect('admin/croptype/all')->with('status', 'Crop Type updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Crop Type');
        }
    }

    public function updateCropTypeImage(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $imageName = 'crop_type_image_'.time().'.'.request()->image->getClientOriginalExtension();
        $storeImage = $request->image->storeAs('images/crop_types', $imageName);
        $croptype = CropType::find($id);
        $croptype->image = $imageName;
        $croptype->save();

        if($croptype) {
            return redirect('admin/croptype/all')->with('status', 'Crop Type Image updated successfully.');
        } else {
            return back()->with('error', 'Failed to update Crop Type Image');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $croptype = CropType::where('id', $id)->delete();
        if($croptype) {
            return redirect('admin/croptype/all')->with('status', 'Crop Type Image deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Crop Type Image');
        }

    }
}

<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Input;
use App\Models\LoanBundle;
use App\Models\LoanBundleInputs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoanBundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loanBundles = LoanBundle::paginate(20);
        $input_ids = [];
        foreach ($loanBundles as $loanBundle) {
            $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $loanBundle->id)->get();
            foreach ($loanBundleInputs as $loanBundleInput) {
                $loanBundle->amount += Input::find($loanBundleInput->input_id)->price * $loanBundleInput->unit;
            }
        }
        if($loanBundles) {
            return view('admin.loan.bundle.index', compact('loanBundles'));
        } else {
            return back()->with('error', 'Loan Bundles not found');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $inputs = Input::all();
        return view('admin.loan.bundle.create', compact('inputs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'inputs' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input_ids = array_values($request->post('inputs'));
        $unit_ids = array_values($request->post('units'));
        $inputs = Input::find($input_ids);
        $price = 0;
        foreach ($inputs as $inp) {
            $price += $inp->price;
        }
        $loanBundle = LoanBundle::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $price,
            'status_id' => 1
        ]);
        if($loanBundle) {
            for ($counter = 0; $counter < sizeof($input_ids); $counter++) {
                LoanBundleInputs::create([
                    'loan_bundle_id' => $loanBundle->id,
                    'input_id' => $input_ids[$counter],
                    'unit' => $unit_ids[$counter]
                ]);
            }
            return redirect('admin/loan/bundle/all')->with('status', 'Loan Bundle created successfully.');
        } else {
            return back()->with('error', 'Failed to create Loan Bundle');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loan_bundle = LoanBundle::where('id', $id)->first();
        $inputs = Input::all();
        $loanBundleInputs = LoanBundleInputs::where('loan_bundle_id', $id)->get();
        $selectedLBIs = [];
        foreach ($loanBundleInputs as $loanBundleInput) {
            array_push($selectedLBIs, ['input_id' => $loanBundleInput->input_id, 'unit' => $loanBundleInput->unit]);
        }

        return view('admin.loan.bundle.edit', compact('loan_bundle', 'inputs', 'selectedLBIs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_ids = array_values($request->post('inputs'));
        $unit_ids = array_values($request->post('units'));
        $inputs = Input::find($input_ids);
        $price = 0;
        foreach ($inputs as $inp) {
            $price += $inp->price;
        }
        $loanBundle = LoanBundle::where('id', $id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $price
        ]);
        $checkForLoanBundleInputRelationship = LoanBundleInputs::where('loan_bundle_id', $id)->get();
        if(sizeof($checkForLoanBundleInputRelationship) > 0) {
            LoanBundleInputs::where('loan_bundle_id', $id)->delete();
        }

        if($loanBundle) {
//            foreach ($input_ids as $iids) {
//                $lbie = LoanBundleInputs::create([
//                    'loan_bundle_id' => $id,
//                    'input_id' => $iids
//                ]);
//            }
            for ($counter = 0; $counter < sizeof($input_ids); $counter++) {
                LoanBundleInputs::create([
                    'loan_bundle_id' => $id,
                    'input_id' => $input_ids[$counter],
                    'unit' => $unit_ids[$counter]
                ]);
            }
            return redirect('admin/loan/bundle/all')->with('status', 'Loan Bundle updated successfully.');
        } else {
            return back()->with('error', 'Failed to updated Loan Bundle');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $loan_bundle = LoanBundle::where('id', $id)->delete();
        if($loan_bundle) {
            return redirect('admin/loan/bundle/all')->with('status', 'Loan Bundle deleted successfully.');
        } else {
            return back()->with('error', 'Failed to delete Loan Bundle');
        }
    }
}

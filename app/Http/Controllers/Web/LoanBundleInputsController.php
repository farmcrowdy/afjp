<?php

namespace App\Http\Controllers\Web;

use App\Models\LoanBundleInputs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanBundleInputsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoanBundleInputs  $loanBundleInputs
     * @return \Illuminate\Http\Response
     */
    public function show(LoanBundleInputs $loanBundleInputs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LoanBundleInputs  $loanBundleInputs
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanBundleInputs $loanBundleInputs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LoanBundleInputs  $loanBundleInputs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanBundleInputs $loanBundleInputs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoanBundleInputs  $loanBundleInputs
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanBundleInputs $loanBundleInputs)
    {
        //
    }
}

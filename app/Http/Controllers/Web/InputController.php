<?php

namespace App\Http\Controllers\Web;

use App\Models\Input;
use App\Models\InputCategory;
use App\Models\InputUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inputs = Input::paginate(20);
        $categories = InputCategory::all();
        $units = InputUnit::all();

        return view('admin.input.index', compact('inputs', 'categories', 'units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = InputCategory::all();
        $units = InputUnit::all();

        return view('admin.input.create', compact('categories', 'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $ic = Input::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'category_id' => $request->category_id,
            'unit_id' => $request->unit_id,
        ]);

        if($ic) {
            return redirect('admin/input/all')->with('status', 'Created Input Successfully.');
        } else {
            return back()->with('error', 'Failed to create Input record');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function show(Input $input)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function edit(int $input_id)
    {
        $input = Input::find($input_id);
        $categories = InputCategory::all();
        $units = InputUnit::all();


        if ($input) {
            return view('admin.input.edit', compact('input', 'categories', 'units'));
        } else {
            return back()->with('error', 'Input not found!.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $input_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $ic = Input::where('id', $input_id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'category_id' => $request->category_id,
            'unit_id' => $request->unit_id,
        ]);

        if($ic) {
            return redirect('admin/input/all')->with('status', 'Updated Input Successfully.');
        } else {
            return back()->with('error', 'Failed to update Input record');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $input_id)
    {
        $input = Input::where('id', $input_id)->delete();
        if($input)  return redirect('admin/input/all')->with('status', 'Deleted Input record successfully.');
        else return back()->with('error', 'Failed to delete Input record');
    }
}

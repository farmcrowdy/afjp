<?php
namespace App\Helpers;

use App\Helper;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SMS {
    private const SENDER = "Farmcrowdy/AFJP";

    /**
     * @param int $to
     * @param int $code
     * @return bool|string
     */
    public static function sendOTP(int $to, int $code)
    {
        $client = (new SMS)->initialize();
//        dd($client);
        if(substr($to, 0, 1) != '+') {
            $to = '+'.$to;
        }
        $site_name = \Helper::config('site_name');
        $emailArray = array(
            // A Twilio phone number you purchased at twilio.com/console
            'from' => \Helper::config('site_phone_number'),
            // the body of the text message you'd like to send
            'body' => "Your $site_name OTP is $code",
        );

        try {
            $client->messages->create(
                $to,
                $emailArray
            );

        } catch (TwilioException $e) {
            Log::info('Error trying to send a message to '.$to.': '.$e);
            return $e->getMessage();
        }

        return true;
    }

    /**
     * @return string|Client
     */
    private function initialize(){

        $sid    = \Helper::config('twilio_sid');
        $token  = \Helper::config('twilio_token');
        try {
            $client = new Client($sid, $token);
        } catch (ConfigurationException $e) {
            return  $e->getMessage();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $client;
    }
}

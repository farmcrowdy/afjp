<?php

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use App\Models\Config;


class Helper extends Model
{
    private const SENDER = "Farmcrowdy";
//if (! function_exists('show_route'))
//{
    function show_route($model, $resource = null)
    {
        $resource = $resource ?? plural_from_model($model);

        return route("{$resource}.show", $model);
    }
//}

    static function config(string $config_name) {
        $configArray = Config::where('name', $config_name)->first();
        if($configArray) {
            return $configArray->value;
        } else {
            return null;
        }
    }

    static function loan_bundle_input_price(int $loan_bundle_id) {
        $inputs = \App\Models\LoanBundleInputs::where('loan_bundle_id', $loan_bundle_id)->get();

        $price = 0;
        if(sizeof($inputs) > 0) {
            foreach ($inputs as $input) {
                $inputObject = \App\Models\Input::find($input->id);
                if(is_object($inputObject)) {
                    $price += $inputObject->price * $input->unit;
                }
            }
        }
        return $price;
    }

    /**
     * this is the function for returning json responses
     * @param $status
     * @param $message
     * @param null $statusCode
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    static function jsonResponse(bool $status, string $message, int $statusCode = null, $data = null){
        //the code means the response code like 200, 404, 400. 419, etc
        $trueRes = [
            'status'=> $status,
            'message'=> $message,
            'data'=>$data
        ];
        $falseRes = [
            'status'=> $status,
            'message'=> $message
        ];
        $finalRes = null;
        $status === true ? $finalRes = $trueRes : $finalRes = $falseRes;
        if(is_null($statusCode)){
            return response()->json($finalRes);
        }
        return response()->json($finalRes, $statusCode);
    }



    /**
     * @param array $fields
     * @return string
     */
     static function checkEmptyFields(array $fields){
        $result = '';
        if(count($fields) > 0){
            foreach($fields as $k => $v){
                if(empty($v) || !isset($v) || is_null($v)){
                    $result = "The $k field is required.";
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * @return int
     */
    static function getMobileOTP(){
        return mt_rand(1000, 9999);
    }

    /**
     * @param string $phone
     * @return \Illuminate\Http\JsonResponse
     */
    public static function validatePhone(string $phone){
        if(strlen($phone) > 11 || strlen($phone) < 11)return \Helper::jsonResponse(false, 'Phone number should contain 11 valid numbers.', 400);
        if(!is_numeric($phone))return \Helper::jsonResponse(false, 'Invalid phone number.', 400);
    }

    /**
     * @param $updated_at
     * @return bool
     */
    public static function validateOTP($updated_at){
        $max_time = 5;//minutes
        $now = Carbon::now();
        $difference = $now->diffInMinutes($updated_at);
        if($difference > $max_time) return true;
        return false;
    }

    /**
     * creates a user's access token
     * @param $UserModel
     * @return string
     */
    static function createAccessToken($UserModel) {
        $accessToken = '';
        try{
            $token = $UserModel->createToken('accessToken')->accessToken;
            if($token){ $accessToken = $token; }
        } catch (Exception $e){ return $accessToken = ''; }
        return $accessToken;
    }

    /**
     * deletes a particular User's earliest tokens
     * @param int $user_id
     */
    public static function deleteEarliestUserAccessToken(int $user_id){
        $data = DB::table("oauth_access_tokens")
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'ASC')
            ->get();

        if(count($data) > 0)return self::finalizeTokenDeletion($data);
    }

    /**
     * deletes all earliest token entries of a particular user
     * @param $tokens
     */
    public static function finalizeTokenDeletion($tokens){
        $total_counts = count($tokens);
        $accessN = ($total_counts - 1); //leave only but the last record
        $Tokens = $tokens; //get the actual data
        $token = [];

        if($accessN > 0){
            for($i = 0; $i < $accessN; $i++){
                $token = $Tokens[$i];
                $id = $token->id;
                $user_id = $token->user_id;

                DB::table('oauth_access_tokens')
                    ->where('id', $id)
                    ->where('user_id', $user_id)
                    ->delete();
            }
        }
    }

    /**
     * @param int $to
     * @param int $code
     * @return bool|string
     */
    public static function sendOTP(int $to, int $code)
    {
        $client = self::initialize();
        if(substr($to, 0, 1) != '+') {
            $to = '+'.$to;
        }
        $site_name = self::config('site_name'); //'Farmcrowdy/AFJP';
        $emailArray = array(
            // A Twilio phone number you purchased at twilio.com/console
            'from' => self::config('site_phone_number'),
            // the body of the text message you'd like to send
            'body' => "Your $site_name OTP is $code",
        );

        try {
            $client->messages->create($to, $emailArray);
        } catch (TwilioException $e) {
            Log::info('Error trying to send a message to '.$to.': '.$e);
            return $e->getMessage();
        }


        return true;
    }

    /**
     * @return string|Client
     */
    private static function initialize(){
        $sid = self::config('twilio_sid'); //env('TWILIO_SID');
        $token = self::config('twilio_token'); //env('TWILIO_TOKEN');
        try {
            $client = new Client($sid, $token);
        } catch (ConfigurationException $e) {
            return  $e->getMessage();
        }

        return $client;
    }
}


<?php

namespace App\Exports;

use App\Models\FarmerLoan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;

class FarmerLoanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return FarmerLoan::all();
//        DB::table('farmer_loans')->join('users', 'users.id', 'farmer_loans.user_id')
    }
}

<?php

namespace App;

use App\Models\State;
use App\Models\UserRole;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'phone_number', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];





    /**
     * @param $columnName
     * @param $value
     * @return mixed
     */
    public static function checkIfExists($columnName, $value){
        return self::where($columnName, $value)->first();
    }

    public function farm_state() {
        return $this->hasOne(State::class, 'id', 'farm_state_id');
    }

    /**
     * @param $password
     * @return mixed
     */
    public static function protectPassword($password){
        return Hash::make($password);
    }

    /**
     * @param string $id_or_phone
     * @return mixed
     */
    public static function getUser(string $id_or_phone_or_email){
        return self::where('id', $id_or_phone_or_email)
            ->orWhere('phone_number', $id_or_phone_or_email)
//            ->orWhere('email', $id_or_phone_or_email)
            ->first();
    }

    /**
     * @param $usrID
     * @param array $fields
     * @return mixed
     */
    public static function updateRecord($usrID, array $fields){
        $User = User::find($usrID);

        if(count($fields) > 0) {
            foreach ($fields as $key => $value) {
                $column = $key;
                $val = $value;

                if($column === 'password') {
                    $User->$column = self::protectPassword($val);
                } else {
                    $User->$column = $val;
                }
            }
            $User->save();
        }
        return $User;
    }

    public function role(){
        return $this->belongsTo(\App\Models\Role::class);
    }

    public function isAdmin(){
        if($this->role->id == 1){
            return false;
        }

        return true;
    }

    public function isHubAdmin(){
        if($this->role->id == 3 || $this->role->id == 1){
            return false;
        }

        return true;
    }
    public function isCentralAdmin(){
        if($this->role->id == 2 || $this->role->id == 1){
            return false;
        }

        return true;
    }
}

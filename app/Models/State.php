<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'country_id', 'status_id',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'id', 'farm_state_id');
    }
}

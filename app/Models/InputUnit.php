<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputUnit extends Model
{
    protected $fillable = ['name', 'notation', 'description'];
}

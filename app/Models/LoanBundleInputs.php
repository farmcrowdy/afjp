<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanBundleInputs extends Model
{
    protected $fillable = ['loan_bundle_id', 'input_id', 'unit'];

//    public function loan_bundles() {
//        return $this->hasMany(LoanBundle::class, 'id', 'loan_bundle_id');
//    }
//
//    public function inputs() {
//        return $this->hasMany(Input::class, 'id', 'input_id');
//    }
}

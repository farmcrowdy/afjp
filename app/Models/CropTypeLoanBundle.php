<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\LoanBundle;

class CropTypeLoanBundle extends Model
{
    use SoftDeletes;

    protected $fillable = ['crop_type_id', 'loan_bundle_id', 'status_id'];

//    public function loan_bundle() {
//        return $this->hasMany(LoanBundle::class, 'loan_bundle_id', 'id');
//    }
}

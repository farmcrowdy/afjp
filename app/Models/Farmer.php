<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Farmer extends Model
{
    use SoftDeletes;

    public function loans() {
        return $this->hasMany(FarmerLoan::class, 'farmer_id', 'id');
    }

    public function crop_type() {
        return $this->hasOne(CropType::class, 'crop_type_id', 'id');
    }

    public function state() {
        return $this->hasOne(State::class, 'id', 'State');
    }

    public function partner() {
        return $this->hasMany(Partner::class, 'Partner', 'id');
    }

    public function partner2() {
        return $this->hasMany(Partner::class, 'Partner2', 'id');
    }
}

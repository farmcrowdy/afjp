<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanBundle extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'description', 'price', 'status_id'
    ];

    public function inputs() {
        return $this->hasMany(LoanBundleInputs::class, 'id', 'loan_bundle_id');
    }

}

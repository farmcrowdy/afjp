<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'status_id', 'farmer_id', 'feedback_text', 'user_id'
    ];
}

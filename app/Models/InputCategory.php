<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputCategory extends Model
{
    protected $fillable = ['name', 'description'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FarmerLoanLoanBundle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'farmer_loan_id', 'loan_bundle_id',
    ];
}

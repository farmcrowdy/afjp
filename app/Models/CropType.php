<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CropType extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'image', 'status_id'];

    public function loan_bundle() {
        return $this->hasMany(LoanBundle::class, 'id', 'loan_bundle_id');
    }
}

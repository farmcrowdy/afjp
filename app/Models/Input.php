<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Input extends Model
{
    protected $fillable = ['name', 'price', 'category_id', 'unit_id', 'description'];

    public function unit() {
        return $this->hasOne(InputUnit::class, 'id', 'unit_id');
    }

    public function category() {
        return $this->hasOne(InputCategory::class, 'id', 'category_id');
    }
}

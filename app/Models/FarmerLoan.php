<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FarmerLoan extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'farmer_id', 'amount', 'status_id', 'user_id', 'repayment_date', 'approved_by_agent_id'
    ];

    public function farmer() {
        return $this->hasOne(Farmer::class, 'id', 'farmer_id');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function status() {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function loan_bundles() {
        return $this->hasMany(FarmerLoanLoanBundle::class, 'id', 'loan_bundle_id');
    }

}

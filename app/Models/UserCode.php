<?php

namespace App\Models;

use App\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCode extends Model
{
    use SoftDeletes;

    public $fillable = ['user_id', 'otp', 'is_used'];

    /**
     * Find a User OTP Code data
     * @param int $otp
     * @return mixed
     */
    public static function findByCode(int $otp){
        return self::where('otp', $otp)->first();
    }

    /**
     * fetch the User code details.
     * @param int $userID
     * @return mixed
     */
    public static function getUserCode(int $userID){
        return UserCode::where('user_id', $userID)->first();
    }

    /**
     * @param int $userID
     * @param string $phone_number
     * @return int
     */
    public static function createNewOTP(int $userID, string $phone_number){
        //create the user's otp code record here
        $otp = \Helper::getMobileOTP();
        $User = self::getUserCode($userID);

        if(!$User) {
            $newOTP = new self();
            $newOTP->user_id = $userID;
            $newOTP->otp = $otp;
            $newOTP->save();

        } else {
            $User->otp = $otp;
            $User->is_used = 0;
            $User->save();
        }

//        \Helper::sendOTP($phone_number, $otp);
        return $otp;
    }

    /**
     * @param int $userID
     */
    public static function updateCodeRec (int $userID){
        $User = self::getTheUserCode($userID);
        if($User){
            $User->is_used = 1;
            $User->save();
        }
    }

    /**
     * fetch the driver code details.
     * @param int $userID
     * @return mixed
     */
    public static function getTheUserCode(int $userID){
        return self::where('user_id', $userID)->first();
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Insurance extends Model
{
    use SoftDeletes;

    public function type() {
        return $this->hasOne(InsuranceType::class, 'insurance_type_id', 'id');
    }
}

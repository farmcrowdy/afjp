<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRepaymentDateToFarmerLoans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('farmer_loans', function (Blueprint $table) {
            $table->timestamp('repayment_date')->nullable();
            $table->unsignedBigInteger('approved_by_agent_id')->nullable();

            $table->foreign('approved_by_agent_id')->references('id')->on('users')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('farmer_loans', function (Blueprint $table) {
            //
        });
    }
}

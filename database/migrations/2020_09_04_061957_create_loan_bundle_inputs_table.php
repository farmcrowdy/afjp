<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanBundleInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_bundle_inputs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loan_bundle_id')->nullable();
            $table->unsignedBigInteger('input_id')->nullable();
            $table->float('unit')->default(1.0)->nullable();
            $table->timestamps();

            $table->foreign('loan_bundle_id')->references('id')->on('loan_bundles')->onDelete('set null')->nullable();
            $table->foreign('input_id')->references('id')->on('inputs')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_bundle_inputs');
    }
}

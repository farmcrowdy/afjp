<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('insurance_type_id')->nullable();
            $table->unsignedBigInteger('farmer_id')->nullable();
            $table->integer('units')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('insurance_type_id')->references('id')->on('insurance_types')->onDelete('set null')->nullable();
            $table->foreign('farmer_id')->references('id')->on('farmers')->onDelete('set null')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}

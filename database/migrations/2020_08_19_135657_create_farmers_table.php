<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('Partner')->nullable();
            $table->unsignedBigInteger('Partner2')->nullable();
            $table->string('Crop')->nullable();
            $table->enum('Gender', ['M', 'F'])->nullable();
            $table->string('First_Name')->nullable();
            $table->string('Middle_Name')->nullable();
            $table->string('Last_Name')->nullable();
            $table->string('Phone')->nullable();
            $table->string('BVN')->nullable();
            $table->unsignedBigInteger('State')->nullable();
            $table->string('NumberOfLivestock')->nullable();
            $table->string('FarmerBVN')->nullable();
            $table->string('LGA')->nullable();
            $table->string('FarmerPhone')->nullable();
            $table->string('Farm_Size_Acres')->nullable();
            $table->string('STATE_LGA')->nullable();
            $table->string('phoneLength')->nullable();
            $table->integer('pin');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('State')->references('id')->on('states')->onDelete(' set null')->nullable();
            $table->foreign('Partner')->references('id')->on('partners')->onDelete(' set null')->nullable();
            $table->foreign('Partner2')->references('id')->on('partners')->onDelete(' set null')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmers');
    }
}

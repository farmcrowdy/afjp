<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmerLoanLoanBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmer_loan_loan_bundles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('farmer_loan_id')->nullable();
            $table->unsignedBigInteger('loan_bundle_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('farmer_loan_id')->references('id')->on('farmer_loans')->onDelete('set null')->nullable();
            $table->foreign('loan_bundle_id')->references('id')->on('loan_bundles')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmer_loan_loan_bundles');
    }
}

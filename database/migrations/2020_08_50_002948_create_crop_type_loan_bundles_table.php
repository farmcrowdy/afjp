<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCropTypeLoanBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crop_type_loan_bundles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('crop_type_id')->nullable();
            $table->unsignedBigInteger('loan_bundle_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('crop_type_id')->references('id')->on('crop_types')->onDelete('set null')->nullable();
            $table->foreign('loan_bundle_id')->references('id')->on('loan_bundles')->onDelete('set null')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crop_type_loan_bundles');
    }
}

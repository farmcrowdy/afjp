<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmerLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmer_loans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('farmer_id')->nullable();
            $table->float('amount')->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('farmer_id')->references('id')->on('farmers')->onDelete('set null')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->nullable();
//            $table->foreign('loan_bundle_id')->references('id')->on('loan_bundles')->onDelete('set null')->nullable();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmer_loans');
    }
}

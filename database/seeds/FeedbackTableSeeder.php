<?php

use Illuminate\Database\Seeder;
use App\Models\Feedback;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fb = new Feedback();
        $fb->feedback_text = 'I just want to write something interesting';
        $fb->user_id = 1;
        $fb->farmer_id = 10;
        $fb->save();
    }
}

<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'Active', 'Inactive', 'Disbursed', 'Repaid', 'Pending', 'Completed', 'Success',
        ];

        foreach ($statuses as $status){
            $statusObject = new \App\Models\Status();
            $statusObject->name = $status;
            $statusObject->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\CropTypeLoanBundle;
use Illuminate\Support\Facades\DB;

class CropTypeLoanBundleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cropTypeLoanBundles = [
            [
                'crop_type_id' => 1,
                'loan_bundle_id' => 1,
            ],
            [
                'crop_type_id' => 1,
                'loan_bundle_id' => 2,
            ],
            [
                'crop_type_id' => 2,
                'loan_bundle_id' => 1,
            ],
            [
                'crop_type_id' => 2,
                'loan_bundle_id' => 2,
            ],
        ];

        foreach ($cropTypeLoanBundles as $cropTypeLoanBundle) {
            $ctlb = new CropTypeLoanBundle();
            $ctlb->crop_type_id = $cropTypeLoanBundle['crop_type_id'];
            $ctlb->loan_bundle_id = $cropTypeLoanBundle['crop_type_id'];
            $ctlb->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            [
                'name' => 'site_name',
                'value' => 'AFJP',
                'status_id' => 1
            ],
            [
                'name' => 'site_url',
                'value' => 'http://localhost:8000',
                'status_id' => 1
            ],
            [
                'name' => 'twilio_sid',
                'value' => 'ACd0f618b73ab06e90ec30c2086d769380',
                'status_id' => 1
            ],
            [
                'name' => 'twilio_token',
                'value' => '48ac78ec30cbaaafbb42793a122602c8',
                'status_id' => 1
            ],
            [
                'name' => 'site_phone_number',
                'value' => '+19564775772',
                'status_id' => 1
            ]
        ];

        foreach ($configs as $config){
            $configObject = new \App\Models\Config();
            $configObject->name = $config['name'];
            $configObject->value = $config['value'];
            $configObject->status_id = $config['status_id'];
            $configObject->save();
        }
    }
}

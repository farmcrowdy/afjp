<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            'Nigeria',
//            'Adamawa', 'Akwa Ibom', 'Anambra', 'Bauchi', 'Bayelsa', 'Benue', 'Borno','Cross River', 'Delta', 'Ebonyi', 'Edo', 'Ekiti', 'Enugu', 'FCT',
            //'Gombe Gombe Imo Owerri Jigawa Dutse Kaduna Kaduna Kano Kano Katsina Katsina Kebbi Birnin Kebbi Kogi Lokoja Kwara Ilorin Lagos Ikeja Nassarawa Lafia Niger Mina Ogun Abeokuta Ondo Akure Osun Osogbo Oyo Ibadan Plateau Jos Rivers Port Harcourt Sokoto Sokoto Taraba Jalingo Yobe Damaturu Zamfara Gusau Read more: https://www.legit.ng/1172078-36-states-capitals-nigeria.html
        ];

        foreach ($countries as $country) {
            $stateObject = new \App\Models\Country();
            $stateObject->name = $country;
            $stateObject->save();
        }
    }
}

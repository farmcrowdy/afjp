<?php

use Illuminate\Database\Seeder;

class InputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inputs = [
            [
                'name' => 'NPK 20:10:10',
                'category_id' => 2,
                'unit_id' => 4,
                'price' => 5600,
                'description' => 'NPK is a type of Fertilizer'
            ],
            [
                'name' => 'UREA',
                'category_id' => 2,
                'unit_id' => 4,
                'price' => 12000,
                'description' => 'UREA is a type of Fertilizer'
            ],
            [
                'name' => 'Hedge',
                'category_id' => 1,
                'unit_id' => 2,
                'price' => 787.5,
                'description' => 'Hedge is a type of Herbicide'
            ],
            [
                'name' => 'Nominee gold',
                'category_id' => 1,
                'unit_id' => 3,
                'price' => 3885,
                'description' => 'Nominee gold is a type of Herbicide'
            ],
            [
                'name' => 'Vanguish',
                'category_id' => 3,
                'unit_id' => 5,
                'price' => 3045,
                'description' => 'Vanguish is a type of Pesticide'
            ],
        ];

        foreach ($inputs as $input) {
            $inp = \App\Models\Input::create([
                'name' => $input['name'],
                'category_id' => $input['category_id'],
                'unit_id' => $input['unit_id'],
                'price' => $input['price'],
                'description' => $input['description']
            ]);
        }
    }
}


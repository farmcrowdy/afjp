<?php

use Illuminate\Database\Seeder;
use App\Models\State;
use Illuminate\Support\Facades\DB;
use App\Models\Farmer;

class FarmerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $farmers = DB::connection('afjp_old')->table('afjp_farmers')->take(100)->get();
        DB::connection('afjp_old')->table('afjp_farmers')->orderBy('First_Name', 'ASC')->take(200)
            ->chunk(100, function($farmers) {
            foreach ($farmers as $farmer)
            {
                $f = new Farmer();
                $f->Partner = $this->getPartner($farmer->Partner);
                $f->Partner2  = $this->getPartner($farmer->Partner2);
                $f->Crop  = $farmer->Crop;
                $f->Gender  = self::fixGender($farmer->Gender);
                $f->First_Name  = $farmer->First_Name;
                $f->Middle_Name  = $farmer->Middle_Name;
                $f->Last_Name  = $farmer->Last_Name;
                $f->Phone  = $farmer->Phone;
                $f->BVN  = $farmer->BVN;
                $f->State  = self::getStateID($farmer->State);
                $f->NumberOfLivestock  = $farmer->NumberOfLivestock;
                $f->FarmerBVN  = $farmer->FarmerBVN;
                $f->LGA  = $farmer->LGA;
                $f->FarmerPhone  = $farmer->FarmerPhone;
                $f->Farm_Size_Acres  = $farmer->Farm_Size_Acres;
                $f->STATE_LGA  = $farmer->STATE_LGA;
                $f->phoneLength  = $farmer->phoneLength;
                $f->pin = $farmer->pin;
                $f->save();
            }
        });
//        foreach ($farmers as $farmer) {
//            $f = new Farmer();
//            $f->Partner = $farmer->Partner;
//            $f->Partner2  = $farmer->Partner2;
//            $f->Crop  = $farmer->Crop;
//            $f->Gender  = $farmer->Gender;
//            $f->First_Name  = $farmer->First_Name;
//            $f->Middle_Name  = $farmer->Middle_Name;
//            $f->Last_Name  = $farmer->Last_Name;
//            $f->Phone  = $farmer->Phone;
//            $f->BVN  = $farmer->BVN;
//            $f->State  = $farmer->State;
//            $f->NumberOfLivestock  = $farmer->NumberOfLivestock;
//            $f->FarmerBVN  = $farmer->FarmerBVN;
//            $f->LGA  = $farmer->LGA;
//            $f->FarmerPhone  = $farmer->FarmerPhone;
//            $f->Farm_Size_Acres  = $farmer->Farm_Size_Acres;
//            $f->STATE_LGA  = $farmer->STATE_LGA;
//            $f->phoneLength  = $farmer->phoneLength;
//            $f->pin = $farmer->pin;
//            $f->save();
//        }
    }

    public function getStateID($statName) {
        $state = DB::table('states')->where('name', $statName)->first();
        if($state){
            return $state->id;
//            dd($state->id);
        }
        else return null;
    }

    public function fixGender($genderString) {
        if($genderString == 'Male') {
            return 'M';
        } elseif($genderString == 'Female') {
            return 'F';
        }
    }

    public function getPartner(string $partner) {
        if($partner == 'Farmcrowdy') {
            return 1;
        } else {
            return 2;
        }
    }

    public function generatePin() {
        return mt_rand(1000, 9999);
    }

}

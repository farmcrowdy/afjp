<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Super Admin',
                'description' => 'Can do all things through Christ that strengthens him.',
                'status_id' => 1
            ],
            [
                'name' => 'Agent',
                'description' => 'Can do some of the things the super admin can do.',
                'status_id' => 1
            ],
            [
                'name' => 'Admins',
                'description' => 'Can just look at things. can not edit.',
                'status_id' => 1
            ],

        ];

        foreach ($roles as $role) {
            $roleObject = new \App\Models\Role();
            $roleObject->name = $role['name'];
            $roleObject->description = $role['description'];
            $roleObject->save();
        }
    }
}

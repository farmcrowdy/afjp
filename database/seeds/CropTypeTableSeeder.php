<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CropTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cropTypes = [
            "tilapia", "cassava", "other", "poultry", "Sorghum", "catfish", "cow", "Maize",
            "oil_palm", "Tomatoes", "soybean", "millet", "cocoa", "groundnuts", "cowpea", "sesame_seeds",
            "carp", "Watermelon", "Tomato", "cotton", "Palmfruit", "Yam", "Pineapple", "Melon",
            "Ginger", "Plantain", "Cocoyam"
        ];

        foreach ($cropTypes as $cropType) {
            $ct = new \App\Models\CropType();
            $ct->name = $cropType;
            $ct->description = 'Crop Type description.';
            $ct->image = 'dummy.jpg';
            $ct->status_id = 1;
            $ct->save();
        }


    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\LoanBundle;
use Illuminate\Support\Facades\DB;

class LoanBundleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loanBundles = [
            [
                'name' => 'Loan Bundle 1',
                'description' => 'Description for loan bundle 1',
                'price' => 1000,
                'status_id' => 1
            ],
            [
                'name' => 'Loan Bundle 2',
                'description' => 'Description for loan bundle 2.',
                'price' => 1000,
                'status_id' => 1
            ],

        ];

        foreach ($loanBundles as $loanBundle) {
            $lb = new LoanBundle();
            $lb->name = $loanBundle['name'];
            $lb->description = $loanBundle['description'];
            $lb->price = $loanBundle['price'];
            $lb->status_id = $loanBundle['status_id'];
            $lb->save();
        }
    }
}

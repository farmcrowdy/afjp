<?php

use Illuminate\Database\Seeder;
use App\Models\InsuranceType;

class InsuranceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insurances = [
            [
                'name' => 'Personal Insurance',
                'description' => 'Tractor, Combiners, Harvesters',
                'price' => 3500,
                'status_id' => 1
            ],
            [
                'name' => 'Crop Protection Insurance',
                'description' => 'Tractor, Combiners, Harvesters',
                'price' => 3500,
                'status_id' => 1
            ],
        ];

        foreach ($insurances as $insurance) {
            $itt = new InsuranceType();
            $itt->name = $insurance['name'];
            $itt->description = $insurance['description'];
            $itt->price = $insurance['price'];
            $itt->status_id = $insurance['status_id'];
            $itt->save();
        }
    }
}

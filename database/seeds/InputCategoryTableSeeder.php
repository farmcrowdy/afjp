<?php

use Illuminate\Database\Seeder;

class InputCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Herbicide',
            ],
            [
                'name' => 'Fertilizer',
            ],
            [
                'name' => 'Pesticide',
            ],
            [
                'name' => 'Boom Spraying',
            ],
            [
                'name' => 'Harvester',
            ],
            [
                'name' => 'Bag',
            ],
        ];

        foreach ($categories as $category) {
            $ic = new \App\Models\InputCategory();
            $ic->name = $category['name'];
            $ic->save();
        }
    }
}

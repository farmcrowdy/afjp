<?php

use Illuminate\Database\Seeder;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partners = [
            [
                'name' => 'Farmcrowdy',
                'status_id' => 1,
            ],
            [
                'name' => 'FMARD_PACE',
                'status_id' => 1,
            ],
        ];

        foreach ($partners as $partner) {
            $po = new \App\Models\Partner();
            $po->name = $partner['name'];
            $po->status_id = $partner['status_id'];
            $po->save();
        }
    }
}

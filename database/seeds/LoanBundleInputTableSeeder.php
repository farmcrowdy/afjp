<?php

use Illuminate\Database\Seeder;

class LoanBundleInputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inputs = \App\Models\Input::all();
        $loan_bundles = \App\Models\LoanBundle::all();
        foreach ($loan_bundles as $loan_bundle) {
            foreach ($inputs as $input) {
                \App\Models\LoanBundleInputs::create([
                   'loan_bundle_id' => $loan_bundle->id,
                    'input_id' => $input->id
                ]);
            }
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class InputUnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
            [
                'name' => 'Kilograms',
                'notation' => 'KG',
                'description' => 'Kilograms',
                ],
            [
                'name' => 'Grams',
                'notation' => 'G',
                'description' => 'Grams',
            ],
            [
                'name' => 'MIlligrams',
                'notation' => 'MG',
                'description' => 'Milligrams',
            ],
            [
                'name' => 'Bag(s)',
                'notation' => 'Bag(s)',
                'description' => 'Bags',
            ],
            [
                'name' => 'Litres',
                'notation' => 'L',
                'description' => 'Litres',
            ],
            [
                'name' => 'Sachets',
                'notation' => 'sachets',
                'description' => 'Sachets',
            ],
        ];

        foreach ($units as $unit) {
            $iu = new \App\Models\InputUnit();
            $iu->name = $unit['name'];
            $iu->notation = $unit['notation'];
            $iu->description = $unit['description'];
            $iu->save();
        }
    }
}

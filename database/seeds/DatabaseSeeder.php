<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(UserSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(ConfigTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(CropTypeTableSeeder::class);
        $this->call(LoanBundleTableSeeder::class);
        $this->call(CropTypeLoanBundleTableSeeder::class);
        $this->call(InsuranceTypeTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(FarmerTableSeeder::class);
        $this->call(InputUnitTableSeeder::class);
        $this->call(InputCategoryTableSeeder::class);
        $this->call(InputTableSeeder::class);
        $this->call(LoanBundleInputTableSeeder::class);
        $this->call(NewFarmersTableSeeder::class);

//        $this->call(FeedbackTableSeeder::class);
    }
}

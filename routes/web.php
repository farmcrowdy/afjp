<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth')->group(function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'Web\AdminController@index')->name('admin');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::group(['prefix' => 'config'], function() {
           Route::get('all', 'Web\ConfigController@index');
           Route::get('create', 'Web\ConfigController@create');
           Route::post('store', 'Web\ConfigController@store');
           Route::get('edit/{config_id}', 'Web\ConfigController@edit');
           Route::post('update/{config_id}', 'Web\ConfigController@update');
           Route::get('delete/{config_id}', 'Web\ConfigController@destroy');

        });

        Route::group(['prefix' => 'role'], function () {
            Route::get('all', 'API\RoleController@index');
            Route::get('create', 'API\RoleController@create');
            Route::post('store', 'API\RoleController@store');
            Route::get('edit/{farmer_loan_id}', 'API\RoleController@edit');
            Route::post('update/{farmer_loan_id}', 'API\RoleController@update');
            Route::get('delete/{farmer_loan_id}', 'API\RoleController@edit');
        });

        Route::group(['prefix' => 'farmer'], function () {
           Route::get('all', 'Web\FarmerController@index')->name('farmers');
           Route::get('create', 'Web\FarmerController@create');
           Route::post('store', 'Web\FarmerController@store');
           Route::get('edit/{farmer_id}', 'Web\FarmerController@edit');
           Route::post('update/{farmer_id}', 'Web\FarmerController@update');
           Route::get('delete/{farmer_id}', 'Web\FarmerController@edit');

           Route::group(['prefix' => 'loan'], function () {
              Route::get('all', 'Web\FarmerLoanController@index');
              Route::get('create', 'Web\FarmerLoanController@create');
              Route::post('store', 'Web\FarmerLoanController@store');
              Route::get('export-loan', 'Web\FarmerLoanController@exportFarmerLoan');
              Route::get('edit/{farmer_loan_id}', 'Web\FarmerLoanController@edit');
              Route::post('update/{farmer_loan_id}', 'Web\FarmerLoanController@update');
              Route::get('delete/{farmer_loan_id}', 'Web\FarmerLoanController@edit');
           });
        });

        Route::group(['prefix' => 'croptype'], function () {
            Route::get('all', 'Web\CropTypeController@index');
            Route::get('edit/{crop_type_id}', 'Web\CropTypeController@edit');
            Route::post('update/{crop_type_id}', 'Web\CropTypeController@update');
            Route::get('create', 'Web\CropTypeController@create');
            Route::post('store', 'Web\CropTypeController@store');
            Route::post('update_croptype_image/{crop_type_id}', 'Web\CropTypeController@updateCropTypeImage');
            Route::get('delete/{crop_type_id}', 'Web\CropTypeController@destroy');
        });

        Route::group(['prefix' => 'input'], function () {
           Route::group(['prefix' => 'unit'], function () {
               Route::get('all', 'Web\InputUnitController@index');
               Route::get('create', 'Web\InputUnitController@create');
               Route::post('store', 'Web\InputUnitController@store');
               Route::get('edit/{input_category_id}', 'Web\InputUnitController@edit');
               Route::post('update/{input_category_id}', 'Web\InputUnitController@update');
               Route::get('delete/{input_category_id}', 'Web\InputUnitController@destroy');
           });

            Route::group(['prefix' => 'category'], function () {
                Route::get('all', 'Web\InputCategoryController@index');
                Route::get('create', 'Web\InputCategoryController@create');
                Route::post('store', 'Web\InputCategoryController@store');
                Route::get('edit/{input_category_id}', 'Web\InputCategoryController@edit');
                Route::post('update/{input_category_id}', 'Web\InputCategoryController@update');
                Route::get('delete/{input_category_id}', 'Web\InputCategoryController@destroy');
            });

            Route::get('all', 'Web\InputController@index');
            Route::get('create', 'Web\InputController@create');
            Route::post('store', 'Web\InputController@store');
            Route::get('edit/{input_id}', 'Web\InputController@edit');
            Route::post('update/{input_id}', 'Web\InputController@update');
            Route::get('delete/{input_id}', 'Web\InputController@destroy');
        });

        Route::group(['prefix' => 'loan'], function () {
            Route::group(['prefix' => 'bundle'], function () {
                Route::get('all', 'Web\LoanBundleController@index');
                Route::get('create', 'Web\LoanBundleController@create');
                Route::post('store', 'Web\LoanBundleController@store');
                Route::get('edit/{input_category_id}', 'Web\LoanBundleController@edit');
                Route::post('update/{input_category_id}', 'Web\LoanBundleController@update');
                Route::get('delete/{input_category_id}', 'Web\LoanBundleController@destroy');
            });
        });

        Route::group(['prefix' => 'user'], function() {
           Route::get('all', 'Web\AdminController@allusers');
            Route::get('create', 'Web\AdminController@create');
            Route::post('store', 'Web\AdminController@store');
            Route::get('edit/{user_id}', 'Web\AdminController@edit');
            Route::post('update/{user_id}', 'Web\AdminController@update');
            Route::get('delete/{user_id}', 'Web\AdminController@destroy');
        });
    });
});


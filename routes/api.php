<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('register', 'Auth\RegisterController@authenticate');
Route::post('login', 'Auth\RegisterController@authenticate');
Route::post('otp/resend', 'Auth\RegisterController@authenticate');
Route::post('otp/verify', 'Auth\RegisterController@verifyOTP');


Route::group(['prefix'=> 'password'], function (){
    Route::group(['prefix' => 'reset'], function () {
        Route::post('/request', 'Auth\RegisterController@requestPasswordChange');//request for a password reset
        Route::post('/otp/verify', 'Auth\RegisterController@verifyPasswordResetOTP'); // Password Reset OTP verification
        Route::post('/update', 'Auth\RegisterController@resetUserPassword');//Update user password.
    });
});

Route::get('loan/bundle/all', 'API\LoanBundleController@index');
Route::post('farmer/loan/repay/{farmer_loan_id}', 'API\FarmerLoanController@repayLoan');

Route::group(['prefix' => 'status'], function () {
    Route::get('all', 'API\StatusController@index');
    Route::get('get/{config_id}', 'API\StatusController@show');
    Route::post('add', 'API\StatusController@store');
    Route::patch('edit/{config_id}', 'API\StatusController@update');
    Route::delete('delete/{config_id}', 'API\StatusController@destroy');
});
Route::group(['prefix' => 'metric'], function() {
    Route::get('farmers-distribution', 'API\FarmerController@farmersDistribution');
});
Route::group(['prefix' => 'config'], function () {
    Route::get('all', 'API\ConfigController@index');
    Route::post('add', 'API\ConfigController@store');
    Route::patch('edit/{config_id}', 'API\ConfigController@update');
    Route::get('get/{config_id}', 'API\ConfigController@show');
    Route::delete('delete/{config_id}', 'API\ConfigController@destroy');
});

Route::group(['prefix' => 'state'], function () {
    Route::get('all', 'API\StateController@index');
    Route::get('get/{state_id}', 'API\StateController@show');
});

Route::middleware('auth:api')->group(function () {
    Route::post('logout', 'Auth\RegisterController@logout');

    Route::group(['prefix' => 'user'], function() {
        Route::get('profile', 'HomeController@user_profile');
        Route::post('profile', 'HomeController@update_profile');
        Route::post('profile/avatar', 'HomeController@uploadAvatar');
        Route::get('profile/avatar', 'HomeController@getAvatar');
        Route::get('dashboard/summary', 'HomeController@dashboardSummary');
        Route::get('farmers/disbursed/today', 'HomeController@farmersDisbursedToday');
        Route::get('farmers/disbursed', 'HomeController@farmersDisbursed');
    });

    Route::group(['prefix'=> 'password'], function (){
        Route::post('/request', 'Auth\RegisterController@requestPasswordChange');//request for a password change
        Route::post('/update', 'Auth\RegisterController@updatePassword');//Update default user password.
        Route::post('/change', 'Auth\RegisterController@changePassword');//take this down soon
    });

    Route::group(['prefix' => 'farmer'], function () {
        Route::get('get/{farmer_phone_number}', 'API\FarmerController@show');
        Route::get('all', 'API\FarmerController@index');
        Route::delete('delete/{farmer_id}', 'API\FarmerController@destroy');
        Route::get('{farmer_id}/pin/{ping}', 'API\FarmerController@confirmPin');


        Route::group(['prefix' => 'loan'], function () {
           Route::get('all', 'API\FarmerLoanController@index');
           Route::post('create', 'API\FarmerLoanController@store');
           Route::get('get/{farmer_loan_id}', 'API\FarmerLoanController@show');
           Route::get('get_by_farmer_id/{loan_bundle_id}', 'API\FarmerLoanController@showFarmerLoan');
           Route::patch('edit/{farmer_loan_id}', 'API\FarmerLoanController@update');
           Route::delete('delete/{farmer_loan_id}', 'API\FarmerLoanController@destroy');
        });
    });

    Route::group(['prefix' => 'croptype'], function () {
        Route::get('all', 'API\CropTypeController@index');
        Route::post('create', 'API\CropTypeController@store');
        Route::post('edit/{crop_type_id}', 'API\CropTypeController@update');
        Route::get('get/{crop_type_id}', 'API\CropTypeController@show');
        Route::delete('delete/{crop_type_id}', 'API\CropTypeController@destroy');
        Route::get('loan_bundle/{crop_type_id}', 'API\CropTypeLoanBundleController@index');
    });

    Route::group(['prefix' => 'feedback'], function () {
        Route::get('all', 'API\FeedbackController@index');
        Route::post('create', 'API\FeedbackController@store');
        Route::get('get/{feedback_id}', 'API\FeedbackController@show');
        Route::get('user/{user_id}', 'API\FeedbackController@showUserFeedback');
        Route::post('update/{feedback_id}', 'API\FeedbackController@update');
        Route::delete('delete/{feedback_id}', 'API\FeedbackController@destroy');

    });

    Route::group(['prefix' => 'loan'], function () {
        Route::group(['prefix' => 'bundle'], function () {
//            Route::get('all', 'API\LoanBundleController@index');
            Route::post('create', 'API\LoanBundleController@store');
            Route::patch('edit/{loan_bundle_id}', 'API\LoanBundleController@update');
            Route::get('get/{loan_bundle_id}', 'API\LoanBundleController@show');
            Route::delete('delete/{loan_bundle_id}', 'API\LoanBundleController@destroy');
       });
    });

    Route::group(['prefix' => 'insurance'], function () {
       Route::group(['prefix' => 'type'], function() {
            Route::get('all', 'API\InsuranceTypeController@index');
            Route::get('get/{insurance_type_id}', 'API\InsuranceTypeController@show');
       });
    });

    Route::group(['prefix' => 'input'], function () {
        Route::group(['prefix' => 'unit'], function () {
            Route::get('all', 'Web\InputUnitController@index');
            Route::get('get/{unit_id}', 'Web\InputUnitController@show');
        });

        Route::group(['prefix' => 'category'], function () {
            Route::get('all', 'Web\InputCategoryController@index');
            Route::get('get/{unit_id}', 'Web\InputCategoryController@show');
        });
    });



});





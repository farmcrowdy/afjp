axios.get('/api/state/all').then(function (response) {
    if(response.data.status === true) {
        let states = response.data.data
        var size = Object.keys(states).length;
        let state_options = `<option value="">Select state</option>`;
        for (let counter = 0; counter < size; counter++) {
            state_options += `<option value="${states[counter].id}">${states[counter].name}</option>`
        }
        console.log($('#states'))

        $('#states').html(state_options)

    }
})
axios.get('/api/metric/farmers-distribution').then(function (response) {
    if(response.data.status === true) {
        let distribution = response.data.data
        let states_value = []
        let states_name = []
        var size = Object.keys(distribution).length;
        for(var counter = 1; counter < size; counter++) {
            states_value.push(distribution[counter].count)
            states_name.push(distribution[counter].name)
        }
        var options = {
            series: [
                {
                    data: states_value
                }
            ],
            chart: {
                type: 'bar',
                height: 700
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: states_name
            }
        };
        var chart = new ApexCharts(document.querySelector("#state_chart"), options);
        chart.render();
    }

})




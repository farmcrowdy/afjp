//add more input for invite friends
function makeRepay(loan_info, user_info) {
    var loan_wrapper = $('.loans'); //Input field wrapper
    // $(loan_wrapper).on('click', '.repaid', function (e) {
        // e.preventDefault();
        // preventDefault();
        // let loan_id = this.id
        let isCertain =confirm('Are you sure you want to mark this loan as repaid? This is a one time action and cannot be reversed.')
        if(isCertain) {
            var form = new FormData();
            form.append("loan_id", loan_info)
            form.append("user_id", user_info)
            var settings = {
                "url": "/api/farmer/loan/repay/" + loan_info,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Accept": "application/json"
                },
                "processData": false,
                "mimeType": "multipart/form-data",
                "contentType": false,
                "data": form
            };

            $.ajax(settings).done(function (response) {
                alert(JSON.parse(response).message)
                location.reload()
            });
        }
    // })
}

$(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    let options = $('#mainSelect').html()
    var fieldHTML = `<div id="addFields">
                         <div class="form-group row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                       <select name="inputs[]" class="form-control" style="width: 100%;" required> ${options}  </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control units" type="number" name="units[]" value="">
                                </div>
                               <div class="col-md-2 remove_button">
                                   <a href="#" class="btn btn-danger btn-sm">-</a>
                                </div>
                            </div>
                        </div>`;

    var x = 1; //Initial field counter is 1
    $(addButton).click(function () {
        //Check maximum number of input fields
        if (x < maxField) {
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


});

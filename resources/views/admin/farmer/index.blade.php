@extends('layouts.admin')

@section('content')

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Farmers Table</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
{{--                        <table id="example1" class="table table-bordered table-striped">--}}
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>State</th>
                                <th>Partner</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($farmers as $farmer)
                                <tr>
                                    <td>{{ $farmer->id }}</td>
                                    <td>{{ $farmer->First_Name }} {{ $farmer->Middle_Name }} {{ $farmer->Last_Name }}</td>
                                    <td>{{ $farmer->Phone ? $farmer->Phone : '' }}</td>
                                    <td>{{ !is_null($farmer->state) ? $farmer->state->name : '' }}</td>
                                    <td>Farmcrowdy</td>
{{--                                    <td><span class="tag tag-success">Approved</span></td>--}}
                                    <td>
                                        <div class="dropdown for-notification">
                                            <button class="btn btn-outline-danger dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-gear"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="notification">
                                                {{--                                                    <a class="dropdown-item media" href="{{ url('admin/payment/show/'.$payment->id)  }}">--}}
                                                {{--                                                        <i class="fa fa-eye"></i>--}}
                                                {{--                                                        <p>View</p>--}}
                                                {{--                                                    </a>--}}
{{--                                                <a class="dropdown-item media" href="{{ url('admin/farmer/edit/'.$farmer->id)  }}">--}}
{{--                                                    <i class="fa fa-wrench"></i>--}}
{{--                                                    <p>View/Edit</p>--}}
{{--                                                </a>--}}
                                                <a class="dropdown-item media" onclick='confirm("Are you sure you want to delete payment #{{ $farmer->id }}?")' href="{{ url('admin/farmer/delete/'.$farmer->id)  }}">
                                                    <i class="fa fa-times"></i>
                                                    <p>Delete</p>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $farmers->links() }}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
            </div>
        </section>
    </div>
@endsection

@section('assets')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            $('#example2').DataTable({
                // "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endsection

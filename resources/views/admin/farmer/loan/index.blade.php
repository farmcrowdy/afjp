@extends('layouts.admin')

@section('content')

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Disbursed Loans Table</h3>

                                <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                {{--                        <table class="table table-hover text-nowrap">--}}
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Farmer</th>
                                        <th>Amount</th>
                                        <th>Agent</th>
                                        <th>Status</th>
                                        <th>Date of Disbursement/Repayment</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($farmerLoans as $loan)
                                        <tr class="loans">
                                            <td>{{ $loan->farmer->First_Name }} {{ $loan->farmer->Middle_Name }} {{ $loan->farmer->Last_Name }}</td>
                                            <td>₦{{ number_format($loan->amount) }}</td>
                                            <td>{{ $loan->user->first_name }} {{ $loan->user->last_name }}</td>
                                            <td>
                                                @if($loan->status->name == 'Disbursed')
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary active">
                                                        <input type="radio" name="status_id" class="disbursed" autocomplete="off" checked> Disbursed
                                                    </label>
                                                    <label class="btn btn-secondary">
                                                        <input type="radio" name="status_id" class="repaid" id="{{ $loan->id }}" onclick="makeRepay(this.id, {{ auth()->id() }})" autocomplete="off"> Repay
                                                    </label>
                                                </div>
                                                @elseif($loan->status->name == 'Repaid')
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary ">
                                                        <input type="radio" name="status_id" autocomplete="off"> Disbursed
                                                    </label>
                                                    <label class="btn btn-secondary active">
                                                        <input type="radio" name="status_id" autocomplete="off" checked> Repaid
                                                    </label>
                                                </div>
                                                @endif
                                            </td>
                                            <td>@if($loan->status->name == 'Disbursed') {{ $loan->created_at->todatestring() }} @elseif($loan->status->name == 'Repaid'){{ $loan->repayment_date }} @endif</td>
{{--                                            <td>@if($loan->status->name == 'Disbursed') <p><span class="badge badge-info">Disbursed</span></p> {{ $loan->created_at->todatestring() }} @elseif($loan->status->name == 'Repaid') <p><span class="badge badge-success">Repaid</span></p> {{ $loan->repayment_date }} @endif</td>--}}

                                            <td>
                                                <div class="dropdown for-notification">
                                                    <button class="btn btn-outline-danger dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-gear"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="notification">
                                                        {{--                                                    <a class="dropdown-item media" href="{{ url('admin/payment/show/'.$payment->id)  }}">--}}
                                                        {{--                                                        <i class="fa fa-eye"></i>--}}
                                                        {{--                                                        <p>View</p>--}}
                                                        {{--                                                    </a>--}}
                                                        <a class="dropdown-item media" href="{{ url('admin/farmer/loan/edit/'.$loan->id)  }}">
                                                            <i class="fa fa-wrench"></i>
                                                            <p>View/Edit</p>
                                                        </a>
                                                        <a class="dropdown-item media" onclick='confirm("Are you sure you want to delete Loan record #{{ $loan->id }}?")' href="{{ url('admin/farmer/loan/delete/'.$loan->id)  }}">
                                                            <i class="fa fa-times"></i>
                                                            <p>Delete</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-12 text-center p-5">
                                    <a class="btn btn-md btn-warning btn-link" href="{{ url('admin/farmer/loan/export-loan') }}">Export Loans</a>
                                </div>
                                {{ $farmerLoans->links() }}
                            </div>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('assets')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endsection

@extends('layouts.admin')

@section('content')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Farmer Loan Form</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">Home</a></li>
                            <li class="breadcrumb-item active">Farmer Loan</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Farmer Loan</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('admin/farmer/loan/update', $farmerLoan->id) }}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <div class="form-group">
                                            <select class="form-control" name="farmer_id" style="width: 100%;" required>
                                                @foreach($farmers as $farmer)
                                                    <option value="{{ $farmer->id }}" @if($farmer->id == old('farmer_id')) selected @elseif($farmer->id == $farmerLoan->farmer_id) selected @endif>{{ $farmer->First_Name }} {{ $farmer->Last_Name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Loan Bundle(s)</label>
                                        <div class="form-group">
                                            <select class="select2bs4" name="loan_bundles[]" multiple="multiple" style="width: 100%;" required data-placeholder="Select a Loan Bundles">
                                                @foreach($loanBundles as $loanBundle)
                                                    <option value="{{ $loanBundle->id }}" @if(in_array($loanBundle->id, $selectedLBIs)) selected @endif>{{ $loanBundle->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="loan_bundles">Loan Agent</label>
                                        <div class="form-group">
                                            <select class="form-control" name="user_id" style="width: 100%;" required>
                                                <option value selected disabled>Select an Agent</option>
                                                @foreach($agents as $agent)
                                                    <option value="{{ $agent->id }}" @if($agent->id == $farmerLoan->user_id) selected @endif>{{ $agent->first_name }} {{ $agent->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="loan_bundles">Loan Status</label>
                                        <div class="form-group">
                                            <select class="form-control" name="status_id" style="width: 100%;" required>
                                                <option value selected disabled>Select a Status</option>
                                                @foreach($statuses as $status)
                                                    <option value="{{ $status->id }}" @if($status->id == $farmerLoan->status_id) selected @endif>{{ $status->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


@section('assets')
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection

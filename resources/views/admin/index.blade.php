@extends('layouts.admin')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Summary</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
{{--        <div class=" container-fluid">--}}
{{--            <div class="ml-2 mr-2 filter_container page_filter">--}}
{{--               <div class="form_group">--}}
{{--                    <label for="state">By State</label>--}}
{{--                    <select name="" id="state">--}}
{{--                        <option value="">Select state</option>--}}
{{--                        @foreach($states as $state)--}}
{{--                            <option value="{{ $state->id }}">{{ $state->name }}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--               </div>--}}
{{--               <div class="form_group">--}}
{{--                <label for="input">By Input</label>--}}
{{--                    <select name="" id="input">--}}
{{--                        <option value="">Select input</option>--}}
{{--                        @foreach($input_list as $input)--}}
{{--                            <option value="{{ $input->id }}">{{ $input->name }}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--               </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ number_format($farmers) }}</h3>
                                <p>Farmers</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person"></i>
                            </div>
                            <a href="{{ url('admin/farmer/all') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3><sup style="font-size: 20px">₦</sup>{{ number_format($loanDisbursed) }}</h3>

                                <p>Loans Disbursed</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{ url('admin/farmer/loan/all') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $agents }}</h3>

                                <p>Agents</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{ number_format($inputs) }}</h3>

                                <p>Inputs</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="{{ url('admin/input/all') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
{{--                    <section class="col-lg-7 connectedSortable">--}}
                        <!-- Custom tabs (Charts with tabs)-->
{{--                        <div class="card">--}}
{{--                            <div class="card-header">--}}
{{--                                <h3 class="card-title">--}}
{{--                                    <i class="fas fa-chart-pie mr-1"></i>--}}
{{--                                    Sales--}}
{{--                                </h3>--}}
{{--                                <div class="card-tools">--}}
{{--                                    <ul class="nav nav-pills ml-auto">--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Area</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <a class="nav-link" href="#sales-chart" data-toggle="tab">Donut</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div> --}}
{{--                            <div class="card-body">--}}
{{--                                <div class="tab-content p-0">--}}
{{--                                    <div class="chart tab-pane active" id="revenue-chart"--}}
{{--                                         style="position: relative; height: 300px;">--}}
{{--                                        <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>--}}
{{--                                    </div>--}}
{{--                                    <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">--}}
{{--                                        <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <!-- /.card -->

                        <!-- DIRECT CHAT -->
{{--                        <div class="card direct-chat direct-chat-primary">--}}
{{--                            <div class="card-header">--}}
{{--                                <h3 class="card-title">Direct Chat</h3>--}}

{{--                                <div class="card-tools">--}}
{{--                                    <span data-toggle="tooltip" title="3 New Messages" class="badge badge-primary">3</span>--}}
{{--                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">--}}
{{--                                        <i class="fas fa-minus"></i>--}}
{{--                                    </button>--}}
{{--                                    <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Contacts"--}}
{{--                                            data-widget="chat-pane-toggle">--}}
{{--                                        <i class="fas fa-comments"></i>--}}
{{--                                    </button>--}}
{{--                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <!-- /.card-header -->
{{--                            <div class="card-body"> --}}
{{--                                <div class="direct-chat-messages"> --}}
{{--                                    <div class="direct-chat-msg">--}}
{{--                                        <div class="direct-chat-infos clearfix">--}}
{{--                                            <span class="direct-chat-name float-left">Alexander Pierce</span>--}}
{{--                                            <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>--}}
{{--                                        </div> --}}
{{--                                        <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image"> --}}
{{--                                        <div class="direct-chat-text">--}}
{{--                                            Is this template really for free? That's unbelievable!--}}
{{--                                        </div> --}}
{{--                                    </div> --}}
{{-- --}}
{{--                                    <div class="direct-chat-msg right">--}}
{{--                                        <div class="direct-chat-infos clearfix">--}}
{{--                                            <span class="direct-chat-name float-right">Sarah Bullock</span>--}}
{{--                                            <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>--}}
{{--                                        </div> --}}
{{--                                        <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image"> --}}
{{--                                        <div class="direct-chat-text">--}}
{{--                                            You better believe it!--}}
{{--                                        </div> --}}
{{--                                    </div> --}}
{{--                                    <div class="direct-chat-msg">--}}
{{--                                        <div class="direct-chat-infos clearfix">--}}
{{--                                            <span class="direct-chat-name float-left">Alexander Pierce</span>--}}
{{--                                            <span class="direct-chat-timestamp float-right">23 Jan 5:37 pm</span>--}}
{{--                                        </div> --}}
{{--                                        <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image"> --}}
{{--                                        <div class="direct-chat-text">--}}
{{--                                            Working with AdminLTE on a great new app! Wanna join?--}}
{{--                                        </div> --}}
{{--                                    </div> --}}
{{-- --}}
{{--                                    <div class="direct-chat-msg right">--}}
{{--                                        <div class="direct-chat-infos clearfix">--}}
{{--                                            <span class="direct-chat-name float-right">Sarah Bullock</span>--}}
{{--                                            <span class="direct-chat-timestamp float-left">23 Jan 6:10 pm</span>--}}
{{--                                        </div> --}}
{{--                                        <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image"> --}}
{{--                                        <div class="direct-chat-text">--}}
{{--                                            I would love to.--}}
{{--                                        </div> --}}
{{--                                    </div> --}}

{{--                                </div>--}}
{{--                                <!--/.direct-chat-messages-->--}}

{{--                                <!-- Contacts are loaded here -->--}}
{{--                                <div class="direct-chat-contacts">--}}
{{--                                    <ul class="contacts-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                <img class="contacts-list-img" src="dist/img/user1-128x128.jpg">--}}

{{--                                                <div class="contacts-list-info">--}}
{{--                          <span class="contacts-list-name">--}}
{{--                            Count Dracula--}}
{{--                            <small class="contacts-list-date float-right">2/28/2015</small>--}}
{{--                          </span>--}}
{{--                                                    <span class="contacts-list-msg">How have you been? I was...</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.contacts-list-info -->--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <!-- End Contact Item -->--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                <img class="contacts-list-img" src="dist/img/user7-128x128.jpg">--}}

{{--                                                <div class="contacts-list-info">--}}
{{--                          <span class="contacts-list-name">--}}
{{--                            Sarah Doe--}}
{{--                            <small class="contacts-list-date float-right">2/23/2015</small>--}}
{{--                          </span>--}}
{{--                                                    <span class="contacts-list-msg">I will be waiting for...</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.contacts-list-info -->--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <!-- End Contact Item -->--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                <img class="contacts-list-img" src="dist/img/user3-128x128.jpg">--}}

{{--                                                <div class="contacts-list-info">--}}
{{--                          <span class="contacts-list-name">--}}
{{--                            Nadia Jolie--}}
{{--                            <small class="contacts-list-date float-right">2/20/2015</small>--}}
{{--                          </span>--}}
{{--                                                    <span class="contacts-list-msg">I'll call you back at...</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.contacts-list-info -->--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <!-- End Contact Item -->--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                <img class="contacts-list-img" src="dist/img/user5-128x128.jpg">--}}

{{--                                                <div class="contacts-list-info">--}}
{{--                          <span class="contacts-list-name">--}}
{{--                            Nora S. Vans--}}
{{--                            <small class="contacts-list-date float-right">2/10/2015</small>--}}
{{--                          </span>--}}
{{--                                                    <span class="contacts-list-msg">Where is your new...</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.contacts-list-info -->--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <!-- End Contact Item -->--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                <img class="contacts-list-img" src="dist/img/user6-128x128.jpg">--}}

{{--                                                <div class="contacts-list-info">--}}
{{--                          <span class="contacts-list-name">--}}
{{--                            John K.--}}
{{--                            <small class="contacts-list-date float-right">1/27/2015</small>--}}
{{--                          </span>--}}
{{--                                                    <span class="contacts-list-msg">Can I take a look at...</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.contacts-list-info -->--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <!-- End Contact Item -->--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                <img class="contacts-list-img" src="dist/img/user8-128x128.jpg">--}}

{{--                                                <div class="contacts-list-info">--}}
{{--                          <span class="contacts-list-name">--}}
{{--                            Kenneth M.--}}
{{--                            <small class="contacts-list-date float-right">1/4/2015</small>--}}
{{--                          </span>--}}
{{--                                                    <span class="contacts-list-msg">Never mind I found...</span>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.contacts-list-info -->--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <!-- End Contact Item -->--}}
{{--                                    </ul>--}}
{{--                                    <!-- /.contacts-list -->--}}
{{--                                </div>--}}
{{--                                <!-- /.direct-chat-pane -->--}}
{{--                            </div>--}}
                            <!-- /.card-body -->
{{--                            <div class="card-footer">--}}
{{--                                <form action="#" method="post">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input type="text" name="message" placeholder="Type Message ..." class="form-control">--}}
{{--                                        <span class="input-group-append">--}}
{{--                                      <button type="button" class="btn btn-primary">Send</button>--}}
{{--                                    </span>--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </div> --}}
{{--                        </div>--}}
                        <!--/.direct-chat -->

                        <!-- TO DO List -->
                        <!-- /.card -->
{{--                    </section>--}}
                    <!-- /.Left col -->
                    <!-- right col (We are only adding the ID to make the widgets sortable)-->
                    <!-- right col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        <section class="section_container container-fluid">
            <div class=" ml-2 mr-2 mr-md-0">
{{--                <div class="semi_domut_chart">--}}
{{--                    <p>Work done</p>--}}
{{--                    <div class="semi_">--}}
{{--                        <!-- starting number   -->--}}
{{--                        <span class="span_ start">0.0k</span>--}}
{{--                        <!-- ending number  -->--}}
{{--                        <span class="span_ end">{{ $farmers/1000 }}k</span>--}}
{{--                        <!-- the style in percent controls the progress of the chart  -->--}}
{{--                        <div class="semi-donut-model" style="--percentage : 70; --fill: #039BE5 ;">--}}
{{--                            {{ $farmers/1000 }}k--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="state_chart">
                <p>Farmers by state</p>
                <!-- state chart  -->
                <div id="state_chart"></div>
            </div>
        </section>
    </div>


@endsection

@extends('layouts.admin')

@section('content')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Loan Bundle Form</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">Home</a></li>
                            <li class="breadcrumb-item active">Loan Bundle</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Loan Bundle</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('admin/loan/bundle/update', $loan_bundle->id) }}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name', $loan_bundle->name) }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control" cols="30"rows="10" required>{{ old('description', $loan_bundle->description) }}</textarea>
                                    </div>
                                    <div class="field_wrapper">
                                        @for($counter = 0; $counter < sizeof($selectedLBIs); $counter++)
                                        <div class="form-group row">
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <select name="inputs[]" class="form-control" style="width: 100%;" required id="mainSelect">
                                                        <option selected disabled>Select Input</option>
                                                        @foreach($inputs as $input)
                                                            <option value="{{ $input->id }}" @if($input->id == $selectedLBIs[$counter]['input_id']) selected @endif>{{ $input->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <input class="form-control units" type="number" name="units[]" value="{{ $selectedLBIs[$counter]['unit'] }}" min="1" required>
                                            </div>
                                            <div class="col-md-2 remove_button">
                                                <a href="#" class="btn btn-danger btn-sm">-</a>
                                            </div>
                                        </div>
                                        @endfor
                                    </div>
                                    <a href="#" class="btn btn-default w-100 add_button">+ Add Input</a>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


@section('assets')
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection
